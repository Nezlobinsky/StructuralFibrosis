#ifndef CONFIGPROCESSOR_H
#define CONFIGPROCESSOR_H

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>


class ConfigProcessor{
    
private:
    std::vector<std::map<std::string, std::string>>& unitsVector;

public:
    ConfigProcessor(std::vector<std::map<std::string, std::string>>& unitsVec);
    int Read(const std::string fileName);

};

#endif
 
