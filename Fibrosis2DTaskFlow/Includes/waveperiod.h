#ifndef WAVEPERIODDETECTOR_H
#define WAVEPERIODDETECTOR_H

#include "dynamicstrategy.h"

#include <map>
#include <string>
#include <vector>
#include <utility>


class WavePeriod : public DynamicStrategy
{
public:
    WavePeriod(std::map<std::string, std::string> config);
    ~WavePeriod();
    //void writeMeshPointsPeriod(std::string fileName);
	void track(double** voltage, double t);
	void write();

private:
    int sideNodes;
    int pointsMeshSize;
    std::string resultsDir;
    std::pair<int, int>* periodPoints;
    std::vector<std::vector<std::pair<double, double>>> timeAndUValuesMesh;
    bool* periodThresholdFlags;

    double computePeriod(std::vector<std::pair<double, double>> timeAndUVec);

};

#endif
