import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import pickle


font = {'size'   : 13}

matplotlib.rc('font', **font)


def read_config(file_path, fib_dict, D_ration):
    fib_deg = None
    fib_sz = None
    with open(file_path, "r") as fl:
        for line in fl:
            if "fibDeg =" in line:
                fib_deg = float(line.split("=")[1])
            if "twigsLen =" in line:
                fib_sz = str(int(float(line.split("=")[1])))
    if fib_sz not in fib_dict:
        fib_dict[fib_sz] = {}
    if fib_deg not in fib_dict[fib_sz]:
        fib_dict[fib_sz][fib_deg] = []
    fib_dict[fib_sz][fib_deg].append(D_ration)


fib_dict = {}

for dir_ in os.listdir(sys.argv[1]):
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue
    print dir_
    D_row = np.loadtxt(os.path.join(sys.argv[1], dir_, "D_ratio.txt"))
    
    read_config(os.path.join(sys.argv[1], dir_, "config.txt"), 
                fib_dict, D_row[0]/D_row[1])

    if D_row[0]/D_row[1] > 3:
        print "-----<"


labels = []
for sz in fib_dict:
    vls_av = []
    vls_mn = []
    vls_mx = []
    vls_wd = []
    for fdeg in sorted(fib_dict[sz].keys()):
        vls_av.append(np.mean(fib_dict[sz][fdeg]))
        vls_mn.append(np.mean(fib_dict[sz][fdeg]) - np.min(fib_dict[sz][fdeg]))
        vls_mx.append(np.max(fib_dict[sz][fdeg]) - np.mean(fib_dict[sz][fdeg]))
        #vls_wd.append([np.min(fib_dict[sz][fdeg]), np.max(fib_dict[sz][fdeg])])
    plt.errorbar(sorted(fib_dict[sz].keys()),
                 vls_av, 
                 yerr=[vls_mn, vls_mx], label="size " + sz)
    #labels.append(plt.plot(fib_dict[sz][0], fib_dict[sz][1], ".", label=sz)[0])
    print sz, vls_av, vls_mn, vls_mx
plt.legend(loc=2)
plt.title("Anisotropy (Diffusion estimation) - chess pattern")
plt.ylabel("D ratio")
plt.xlabel("Fibrosis area, %")
plt.grid()
plt.show()
