#include <cmath>
#include <utility> 
#include <algorithm>
#include <iostream>

#include "wavefrontdetector.h"
#include "toolset.h"
 
using std::vector;
using std::tuple;
using std::pair;
using std::make_pair;
using std::make_tuple;
using std::sort;


WaveFrontDetector::WaveFrontDetector(int sideNodes_, double dr_) : sideNodes(sideNodes_),
		                                                           dr(dr_)
{
}

void WaveFrontDetector::setTargetMesh(double** array)
{
    targetVec.resize(sideNodes);
    for (int i = 0; i < sideNodes; ++i){
        targetVec[i].resize(sideNodes);
        for (int j = 0; j < sideNodes; ++j){
            targetVec[i][j] = array[i][j];    
        }
    }
}

void WaveFrontDetector::setTargetMesh(std::vector<std::vector<double>> vec)
{
    targetVec = vec;
    sideNodes = vec.size();
}

void WaveFrontDetector::setWaveDirection(WAVE_DIRECTION waveDirection_)
{
    waveDirection = waveDirection_;
}

vector<pair<int, int>> WaveFrontDetector::findWaveFront()
{          
    vector<pair<int, int>> frontCoordinates;
    
    rainPoints(frontCoordinates);
    
    sortFrontCoordinates(frontCoordinates);
    
    vector<tuple<pair<int, int>, pair<int, int>, int>> specialPointsContainer = findSpecialPoints(frontCoordinates);
    
    int maxItersCount = 2; // front refinement deep
    int iters = 0;
    while (specialPointsContainer.size() && iters < maxItersCount){
        int newElements = 0; // to store the number of added elements
        for (int i = 0; i < specialPointsContainer.size(); ++i){
            pair<int, int> point1 = std::get<0>(specialPointsContainer[i]);
            pair<int, int> point2 = std::get<1>(specialPointsContainer[i]);
            int index             = std::get<2>(specialPointsContainer[i]) + newElements;
            
            vector<double> vec_     = {static_cast<double> (point2.first - point1.first),
                                    static_cast<double> (point2.second - point1.second)};        
            vector<double> vec      = normVector(vec_);
            vector<double> orthoVec = orthoVec2D(vec);
            //NOTE: may be need to check directon correctness here 
            if (waveDirection == WAVE_DIRECTION::NEGATIVE_Y ||
                waveDirection == WAVE_DIRECTION::NEGATIVE_X){
                orthoVec = reverseVec(orthoVec);    
            }
            
            pair<double, double> point1d = make_pair(static_cast<double> (point1.first),
                                                    static_cast<double> (point1.second));

            while(true){
                point1d                           = towardToVec(point1d, vec);
                pair<double, double> segmentPoint = point1d;

                while(true){
                segmentPoint = towardToVec(segmentPoint, orthoVec); 
                int y = round(segmentPoint.first);
                int x = round(segmentPoint.second);
                if (!(sideNodes > x && x > 0 &&
                        sideNodes > y && y > 0)){
                    break;
                }
                if (targetVec[y][x] >= 0.){
                    frontCoordinates.insert(frontCoordinates.begin()+index+1, make_pair(y, x));
                    index++;
                    newElements++;
                    break;
                }
                }
                int y1 = round(point1d.first);
                int x1 = round(point1d.second);
                int y2 = point2.first;
                int x2 = point2.second;            
                
                if (x1 == x2 && y1 == y2){
                    break;
                }
            }
        }
        specialPointsContainer = findSpecialPoints(frontCoordinates);
        iters++;
    }

    return frontCoordinates;
}

void WaveFrontDetector::rainPoints(std::vector<std::pair<int, int>>& frontCoordinates)
{
    if (waveDirection == WAVE_DIRECTION::POSITIVE_X){
        for (int i = sideNodes-1; i > 0; --i){
            for (int j = sideNodes-1; j > 0; --j){
                if (targetVec[i][j-1] >= 0){
                    frontCoordinates.push_back(make_pair(i, j-1));
                    break;
                }
            }
        }
    }
    else if (waveDirection == WAVE_DIRECTION::POSITIVE_Y){
        for (int j = sideNodes-1; j > 0; --j){
            for (int i = sideNodes-1; i > 0; --i){
                if (targetVec[i-1][j] >= 0){
                    frontCoordinates.push_back(make_pair(i-1, j));
                    break;
                }
            }
        }
    }
    else if (waveDirection == WAVE_DIRECTION::NEGATIVE_X){
        for (int i = 0; i < sideNodes-1; ++i){
            for (int j = 0; j < sideNodes-1; ++j){
                if (targetVec[i][j+1] >= 0){
                    frontCoordinates.push_back(make_pair(i, j+1));
                    break;
                }
            }
        }
    }
    else if (waveDirection == WAVE_DIRECTION::NEGATIVE_Y){
        for (int j = 0; j < sideNodes-1; ++j){
            for (int i = 0; i < sideNodes-1; ++i){
                if (targetVec[i+1][j] >= 0){
                    frontCoordinates.push_back(make_pair(i+1, j));
                    break;
                }
            }
        }
    }
}

void WaveFrontDetector::sortFrontCoordinates(vector<pair<int, int>>& frontCoordinates)
{
    //sort pairs by first (i) or second (j) argument:
    if (waveDirection == WAVE_DIRECTION::NEGATIVE_X || waveDirection == WAVE_DIRECTION::POSITIVE_X){ // y stim direction
        std::sort(frontCoordinates.begin(), frontCoordinates.end(), 
            [](const pair<int, int>& coord, const pair<int, int>& nextCoord) { return coord.first < nextCoord.first; });
    }
    else if (waveDirection == WAVE_DIRECTION::POSITIVE_Y || waveDirection == WAVE_DIRECTION::NEGATIVE_Y){ // x stim direction
        std::sort(frontCoordinates.begin(), frontCoordinates.end(), 
            [](const pair<int, int>& coord, const pair<int, int>& nextCoord) { return coord.second < nextCoord.second; });
    }
}

vector<tuple<pair<int, int>, pair<int, int>, int>> WaveFrontDetector::findSpecialPoints(vector<pair<int, int>> frontCoordinates)
{
    vector<tuple<pair<int, int>, pair<int, int>, int>> specialPointsContainer;

    double thrDist = 6.;
    for (int i = 0; i < frontCoordinates.size()-1; ++i){
        if (distance(frontCoordinates[i], frontCoordinates[i+1]) > thrDist){
            specialPointsContainer.push_back(make_tuple(frontCoordinates[i],
                                                        frontCoordinates[i+1],
                                                        i));
        }
    }
    
    return specialPointsContainer;
}

double WaveFrontDetector::distance(pair<int, int> p1, pair<int, int> p2)
{
    return sqrt(pow(p1.first - p2.first, 2) + pow(p1.second - p2.second, 2));
}

vector<double> WaveFrontDetector::normVector(vector<double> vec)
{
    // 2d vector
    double norm = sqrt(pow(vec[0], 2) + pow(vec[1], 2));
    vec[0] /=norm;
    vec[1] /=norm;
    
    return vec;
}
    
vector<double> WaveFrontDetector::orthoVec2D(vector<double> vec)
{
    double vy = vec[0];
    double vx = vec[1];
    
    vec[0] = -vx; 
    vec[1] = vy;
    
    return vec;
}

vector<double> WaveFrontDetector::reverseVec(vector<double> vec)
{
    vec[0] = -vec[0];
    vec[1] = -vec[1];
    
    return vec;
}

pair<double, double> WaveFrontDetector::towardToVec(pair<double, double> point, vector<double> vec)
{
    point.first  += vec[0];
    point.second += vec[1];
    
    return point;
}
