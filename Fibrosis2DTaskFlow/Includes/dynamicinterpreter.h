#ifndef DYNAMICINTERPRETER_H
#define DYNAMICINTERPRETER_H

#include "toolset.h"
#include <vector>
#include <map>
#include <string>
#include <utility>
#include "dynamicstrategy.h"


class DynamicInterpreterContext
{
public:
	//DynamicInterpreterContext();
	void setDynamicStrategy(DynamicStrategy* dynamicStrategy){
		dynamicInterpreterStrategy = dynamicStrategy;
	}

	void execute(double** voltage, double t=0.){
		dynamicInterpreterStrategy->track(voltage, t);
	}

	void write(){
		dynamicInterpreterStrategy->write();
	}

private:
    DynamicStrategy* dynamicInterpreterStrategy;

};


class DynamicInterpreter{
/* 
 * Interpreter that works during the model calculations.
 * All measurements that must be performed during the model calculations should be implemented here. 
 * 
 * Responsible for:
 * Wave period calculation.
 * Making animation frames.
 * Writing potential map and arrival time map at the end of calculations.
 */
private:
	DynamicInterpreterContext activationMapContext;
	DynamicInterpreterContext potentialMapContext;
	DynamicInterpreterContext potentialMapAnimationContext;
	DynamicInterpreterContext wavePeriodContext;
	DynamicInterpreterContext cvRestitutionContext;

    bool periodWriterFlag;
    bool frameWriterFlag;
    bool cvRestitutionFlag;

public:
    DynamicInterpreter(std::map<std::string, std::string> config);
    ~DynamicInterpreter();

    void setInterpreterCalcParameters(std::map<std::string, std::string> config);
    void setInterpreterModes(std::vector<std::string> interpretModes, std::map<std::string, std::string> config);
    
/* Results writers: */
    void interpret(double t, double** voltage, double dt);
    void writeResults(double** voltage);
};

#endif
