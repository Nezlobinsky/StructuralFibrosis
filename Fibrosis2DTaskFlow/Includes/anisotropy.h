/*
 * anisotropy.h
 *
 *  Created on: Mar 1, 2018
 *      Author: collapse
 */

#ifndef ANISOTROPY_H
#define ANISOTROPY_H

#include <map>
#include <string>
#include "staticstrategy.h"


class Anisotropy : public StaticStrategy
{
public:
	Anisotropy(std::map<std::string, std::string> unitMap);
	virtual ~Anisotropy();

	void compute();
};

#endif /* ANISOTROPY_H */
