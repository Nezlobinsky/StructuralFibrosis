#include "dir_installation.h"

#include <vector>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using boost::filesystem::exists;
using boost::filesystem::create_directory;
using boost::starts_with;

using std::string;
using std::map;
using std::vector;
using std::ofstream;
using std::endl;


int DirInstallation::createDirs(map<string, string> &config)
{   
    if(!(exists(config["sectionName"]))){
        if (create_directory(config["sectionName"])){
        }
    }

    if(!(exists(config["dirPath"]))){
        if (create_directory(config["dirPath"])){
        }
    }

    return writeConfig(config);
}


int DirInstallation::writeConfig(map<string, string> &config)
{
    // try to handle map key error
    ofstream outstr(config["dirPath"] + "/config.txt");
    if (outstr)
    {
        map<string, string>::iterator iter;
        for (auto &elem : config){
            outstr << elem.first << " = " << elem.second << endl;
        }
    }
    else{
    	return 1;
    }
    outstr.close();

    return 0;
}
