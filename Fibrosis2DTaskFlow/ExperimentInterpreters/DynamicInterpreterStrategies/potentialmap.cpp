#include "potentialmap.h"
#include "toolset.h"

#include <fstream>
#include <iostream>

using std::map;
using std::string;
using std::ofstream;
using std::endl;


PotentialMap::PotentialMap(std::map<std::string, std::string> config)
{
    sideNodes = ToolSet::stringToInt(config["sideNodes"], "In sideNodes (PotentialMap constructor)");
    transversalActFileFlag = ToolSet::stringToInt(config["TransvActFile"],
        		                                      "In TransvActFile (ActivationMap constructor)");
    resultsDir = config["dirPath"];
}

PotentialMap::~PotentialMap()
{
}

void PotentialMap::track(double** voltage, double t)
{
    potentialMap = voltage;
}

void PotentialMap::write()
{
    string fileName;
    if (!transversalActFileFlag)
        fileName = "/potentialMap.txt";
    else
        fileName = "/potentialMap2.txt";

    ofstream pMap(resultsDir + fileName);
    for (int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j)
            pMap << potentialMap[i][j] << " ";
        pMap << endl;
    }
    pMap.close();
}
