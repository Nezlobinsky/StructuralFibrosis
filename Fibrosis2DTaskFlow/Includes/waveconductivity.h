
#ifndef WAVECONDUCTIVITY_H
#define WAVECONDUCTIVITY_H

#include <map>
#include <string>
#include "staticstrategy.h"


class WaveConductivity : public StaticStrategy
{
public:
	WaveConductivity(std::map<std::string, std::string> unitMap);
	virtual ~WaveConductivity();

	void compute();
};

#endif /* WAVECONDUCTIVITY_H */
