#ifndef POTENTIALMAPANIMATION_H
#define POTENTIALMAPANIMATION_H

#include "dynamicstrategy.h"

#include <map>
#include <string>


class PotentialMapAnimation : public DynamicStrategy {
public:
	PotentialMapAnimation(std::map<std::string, std::string> config);
	virtual ~PotentialMapAnimation();

	void track(double** voltage, double t);
	void write();

private:
	int sideNodes;
	std::string resultsDir;
	double** potentialMap;
	int frameNumber;
	double time;
	double dt;
	double frameStep;

};

#endif /* POTENTIALMAPANIMATION_H */
