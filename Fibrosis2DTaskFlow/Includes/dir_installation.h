#ifndef DIR_INSTALLATION_H
#define DIR_INSTALLATION_H

#include <map>
#include <vector>
#include <iostream>
#include <fstream>


#include "toolset.h"


class DirInstallation{
/*
 * Create experiments directories (with nested directories per each task). 
 */
public:
    int createDirs(std::map <std::string, std::string> &config);
    int writeConfig(std::map <std::string, std::string> &config);
};

#endif //DIR_INSTALLATION_H
