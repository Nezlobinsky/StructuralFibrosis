import sys
import copy
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

font = {'size'   : 13}

matplotlib.rc('font', **font)


size_dict = {1: [[], [], [], [], []], 
             2: [[], [], [], [], []], 
             3: [[], [], [], [], []], 
             4: [[], [], [], [], []]}



with open(sys.argv[1]) as rfile:
    for line in rfile:
        line = line.split()
        size  = int(float(line[0].split(".")[0]))
        prob  = int(float(line[1])*100)
        value = float(line[2])
        max_v = float(line[3])
        min_v = float(line[4]) 
        
        if size != 1 :
            vl = 1
            #mx = 1
            #mn = 1
            for i, prb in enumerate(size_dict[1][0]):
                if prb == prob:
                    vl = size_dict[1][1][i]
                    #if size_dict[1][2][i] > 0.01:
                    #    mx = size_dict[1][2][i]
                    #if size_dict[1][3][i] > 0.01:
                    #    mn = size_dict[1][3][i]
            size_dict[size][0].append(prob)
            size_dict[size][1].append(value/vl)
            size_dict[size][2].append((vl/max_v) - (vl/value))
            size_dict[size][3].append((vl/value) - (vl/min_v))
        else:
            size_dict[size][0].append(prob)
            size_dict[size][1].append(value)
            size_dict[size][2].append(max_v)
            size_dict[size][3].append(min_v)


for i in range(len(size_dict[1][0])):
    size_dict[1][2][i] = (size_dict[1][1][i]/size_dict[1][2][i]) - 1. 
    size_dict[1][3][i] = 1. - (size_dict[1][1][i]/size_dict[1][3][i]) 
    size_dict[1][1][i] = 1.


if len(sys.argv) > 2:
    with open(sys.argv[2]) as rfile:
        for line in rfile:
            line = line.split()
            size  = line[0].split(".")[0]
            value = float(line[2])*100
                
            size_dict[size][4].append(value)
    
    for key in size_dict:
        plt.errorbar(size_dict[key][4],
                     size_dict[key][1], 
                     yerr=[size_dict[key][2], size_dict[key][3]], label="size " + key)

else:
    for key in size_dict:
        plt.errorbar(size_dict[key][0],
                     size_dict[key][1], 
                     yerr=None, label="size " + str(key))

plt.legend(loc=0)
plt.title("Wave velocity (transversal)")
plt.ylabel("Velocity (mm/ms)")
#plt.xlabel("Obstacles")
plt.xlabel("Fibrosis area, %")
plt.grid()
plt.show()
