import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import os


font = {'size'   : 13}

matplotlib.rc('font', **font)


def read_size(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("twigsLen"):
                return int(float(line_content[1].strip("\n")))


fib_dict = {}

for dir_ in os.listdir(sys.argv[1]):
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue
    print dir_
    objs = np.loadtxt(os.path.join(sys.argv[1], dir_, "objects.txt"))
    
    read_config(os.path.join(sys.argv[1], dir_, "config.txt"), 
                fib_dict, int(objs))


labels = []
for sz in fib_dict:
    vls_av = []
    vls_mn = []
    vls_mx = []
    vls_wd = []
    for fdeg in sorted(fib_dict[sz].keys()):
        vls_av.append(np.mean(fib_dict[sz][fdeg]))
        vls_mn.append(np.mean(fib_dict[sz][fdeg]) - np.min(fib_dict[sz][fdeg]))
        vls_mx.append(np.max(fib_dict[sz][fdeg]) - np.mean(fib_dict[sz][fdeg]))
        #vls_wd.append([np.min(fib_dict[sz][fdeg]), np.max(fib_dict[sz][fdeg])])
    plt.errorbar(sorted(fib_dict[sz].keys()),
                 vls_av, 
                 yerr=[vls_mn, vls_mx], label="size " + sz)
    #labels.append(plt.plot(fib_dict[sz][0], fib_dict[sz][1], ".", label=sz)[0])
    print sz, vls_av, vls_mn, vls_mx
plt.legend(loc=2)
plt.title("Number of obstacles")
plt.ylabel("Obstacles")
plt.xlabel("Fibrosis area, %")
plt.grid()
plt.show()
