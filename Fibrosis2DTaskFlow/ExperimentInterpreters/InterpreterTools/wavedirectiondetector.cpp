#include "wavedirectiondetector.h"
#include "toolset.h"

using std::string;


void WaveDirectionDetector::setStimCoordinates(string str)
/**
 * str form: "y0:y1:x0:x1"
 */
{
    stimCoordinatesVec = ToolSet::splitToInt(str, ":");
}

void WaveDirectionDetector::transversal()
{
    int tmp = stimCoordinatesVec[2];
    stimCoordinatesVec[2] = stimCoordinatesVec[0];
    stimCoordinatesVec[0] = tmp;
    
    tmp = stimCoordinatesVec[3];
    stimCoordinatesVec[3] = stimCoordinatesVec[1];
    stimCoordinatesVec[1] = tmp;
}

WAVE_DIRECTION WaveDirectionDetector::define2DStimulusDirection()
{
    int y0 = stimCoordinatesVec[0];
    int y1 = stimCoordinatesVec[1];
    int x0 = stimCoordinatesVec[2];
    int x1 = stimCoordinatesVec[3];
    
    if (y0 == 0 && y1 > x1){
        return WAVE_DIRECTION::POSITIVE_X;
    }
    else if (x0 == 0 && x1 >= y1){
        return WAVE_DIRECTION::POSITIVE_Y;
    }
    else if ((x1 - x0) > (y1 - y0)){
        return WAVE_DIRECTION::NEGATIVE_Y;
    }
    else{
        return WAVE_DIRECTION::NEGATIVE_X;
    }
}
