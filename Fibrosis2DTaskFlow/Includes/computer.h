#ifndef COMPUTER_H
#define COMPUTER_H

#include <vector>
#include <map>
#include <string>

#include "modelinterface2d.h"
#include "staticinterpreter.h"
#include "dynamicinterpreter.h"
#include "toolset.h"


class Computer
/*
 * Class to prepare and perform all calculations.
 * 
 * Responsible for:
 * Models declaration and initialization (with data from preprocessing classes).
 * Threads initialization (with openmp support).
 * Interpreters (static and dynamic) declaration and initialization (with data from preprocessing classes).
 * Launching calculations for the formed queue (vector) of tasks (std::vector tasksVec)  
 */
{
private:
    int taskThreads;
    int computationalThreads;
    std::vector<ModelInterface2D*> tasksVec;
    std::vector<StaticInterpreter*> staticInterpreterVec;
    std::vector<DynamicInterpreter*> dynamicInterpreterVec;

public:
    void setNumberOfThreads(int taskThreads, int computationalThreads);
    void setTasks(std::vector<std::map<std::string, std::string>> configVec, std::vector<std::vector<std::vector<int>>> fibMatrixVec);
    
    void setStaticInterpreters(std::vector<std::map<std::string, std::string>> unitsVec);
    void setDynamicInterpreters(std::vector<std::map<std::string, std::string>> configVec);
    void bindDynamicInterpreters();
    void run();
    void runStaticInterpreter();
};

#endif
