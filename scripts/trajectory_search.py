import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def read_size(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("twigsLen"):
                return int(float(line_content[1].strip("\n")))

def read_fibDeg(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("fibDeg"):
                return float(line_content[1].strip("\n"))

def spread_out_4p(matr, fibr, n, step):
    up = np.roll(matr, 1, 0)
    down = np.roll(matr, -1, 0)
    right = np.roll(matr, 1, 1)
    left = np.roll(matr, -1, 1)
    sm = up+down+right+left
    sm[:, 0] = 0
    sm[:, -1] = 0
    sm[0, :] = 0
    sm[-1, :] = 0
    new_m = np.where(np.logical_and(np.logical_and(sm > 0, fibr == 0), matr == 0), step, 0)
    matr[new_m == step] = step
    return matr

def spread_out_8p(matr, fibr, n, step):
    up = np.roll(matr, 1, 0)
    down = np.roll(matr, -1, 0)
    right = np.roll(matr, 1, 1)
    left = np.roll(matr, -1, 1)
    up_right = np.roll(up, 1, 1)
    up_left = np.roll(up, -1, 1)
    down_right = np.roll(down, 1, 1)
    down_left = np.roll(down, -1, 1)
    sm = up+down+right+left+up_right+up_left+down_right+down_left
    sm[:, 0] = 0
    sm[:, -1] = 0
    sm[0, :] = 0
    sm[-1, :] = 0
    new_m = np.where(np.logical_and(np.logical_and(sm > 0, fibr == 0), matr == 0), step, 0)
    matr[new_m == step] = step
    return matr

def check_if_reached(last_line):
    if last_line[last_line > 0].size > 0:
        return True
    return False


############
n = 400

data_dict = {}

for dir_ in os.listdir(sys.argv[1]):
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue
    print dir_
    
    fibDg = read_fibDeg(os.path.join(sys.argv[1], dir_, "config.txt"))
    fibSz = read_size(os.path.join(sys.argv[1], dir_, "config.txt"))
    if not fibSz in data_dict:
        data_dict[fibSz] = {}
    if not fibDg in data_dict[fibSz]:
        data_dict[fibSz][fibDg] = []

    fib_matrix = np.loadtxt("fibrosis.txt")

    res_matrix = np.copy(fib_matrix)

    res_matrix[fib_matrix == 1] = 0
    res_matrix[:1, :] = 1

    res_matrix = res_matrix.astype(int)
    fib_matrix = fib_matrix.astype(int)

    iters1 = 1
    while not check_if_reached(res_matrix[398, :]) and iters < 2000:
        res_matrix = spread_out_8p(res_matrix, fib_matrix,  n, iters)
        iters1 += 1

    fib_matrix = np.loadtxt("fibrosis.txt")

    res_matrix = np.copy(fib_matrix)

    res_matrix[fib_matrix == 1] = 0
    res_matrix[:, :1] = 1

    res_matrix = res_matrix.astype(int)
    fib_matrix = fib_matrix.astype(int)

    iters2 = 1
    while not check_if_reached(res_matrix[:, 398]) and iters < 2000:
        res_matrix = spread_out_8p(res_matrix, fib_matrix,  n, iters)
        iters2 += 1

    data_dict[fibSz][fibDg].append(float(iters2)/iters1)  

with open(os.path.join(sys.argv[1], "trajectory_script.txt"), "w") as fl:
    for szKey in sorted(data_dict):
        for fdKey in sorted(data_dict[szKey]):
            fl.write(str(szKey) + " " + str(fdKey) + " " + str(np.mean(data_dict[szKey][fdKey])) + " " 
                     + str(np.max(data_dict[szKey][fdKey])) + " " + str(np.min(data_dict[szKey][fdKey])) + "\n")


