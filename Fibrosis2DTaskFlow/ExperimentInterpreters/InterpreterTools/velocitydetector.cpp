#include <algorithm>
#include <iostream>

#include "velocitydetector.h"
#include "toolset.h"

using std::vector;
using std::pair;


VelocityDetector::VelocityDetector(int sideNodes_, double dr_) : sideNodes(sideNodes_), dr(dr_)
{
}

void VelocityDetector::setTargetMesh(double** array)
{
    targetVec.resize(sideNodes);
    for (int i = 0; i < sideNodes; ++i){
        targetVec[i].resize(sideNodes);
        for (int j = 0; j < sideNodes; ++j){
            targetVec[i][j] = array[i][j];    
        }
    }
}

void VelocityDetector::setTargetMesh(std::vector<std::vector<double>> vec)
{
    targetVec = vec;
    sideNodes = vec.size();
}

void VelocityDetector::setSpatialStep(double dr_)
{
    dr = dr_;
}

void VelocityDetector::setWaveDirection(WAVE_DIRECTION waveDirection_)
{
    waveDirection = waveDirection_;
}

double VelocityDetector::computeVelocity(vector<pair<int, int>> frontVec, double time, double activationTime, bool arrivalTime)
{
    vector<double> velocityVec;
    double threshold = 0.;
    
    for (auto& coordinates : frontVec){
        if (waveDirection == WAVE_DIRECTION::POSITIVE_Y){
            int i = coordinates.first;
            int j = coordinates.second;
            if (targetVec[i][j] > threshold){
                if (arrivalTime){
                    velocityVec.push_back(dr*i/targetVec[i][j]);
                }
                else{
                    velocityVec.push_back(dr*i/(time - activationTime));
                }
            }
        }
        else if (waveDirection == WAVE_DIRECTION::POSITIVE_X){
            int i = coordinates.first;
            int j = coordinates.second;
            if (targetVec[i][j] > threshold){
                if (arrivalTime){
                    velocityVec.push_back(dr*j/targetVec[i][j]);
                }
                else{
                    velocityVec.push_back(dr*j/(time - activationTime));
                }
            }
        }
        else if (waveDirection == WAVE_DIRECTION::NEGATIVE_X){
            int i = coordinates.first;
            int j = coordinates.second;
            if (targetVec[i][j] > threshold){
                if (arrivalTime){
                    velocityVec.push_back(dr*(sideNodes - j)/targetVec[i][j]);
                }
                else{
                    velocityVec.push_back(dr*(sideNodes - j)/(time - activationTime));
                }
            }
        }
        else if (waveDirection == WAVE_DIRECTION::NEGATIVE_Y){
            int i = coordinates.first;
            int j = coordinates.second;
            if (targetVec[i][j] > threshold){
                if (arrivalTime){
                    velocityVec.push_back(dr*(sideNodes - i)/targetVec[i][j]);
                }
                else{
                    velocityVec.push_back(dr*(sideNodes - i)/(time - activationTime));
                }
            }
        }
    }

    // calculate mean
    if (velocityVec.size() > 0)
        return std::accumulate(velocityVec.begin(), velocityVec.end(), 0.)/velocityVec.size();
    else 
        return 0.;
}
