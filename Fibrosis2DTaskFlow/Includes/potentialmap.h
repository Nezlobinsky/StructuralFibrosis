#ifndef POTENTIALMAP_H
#define POTENTIALMAP_H

#include "dynamicstrategy.h"

#include <map>
#include <string>


class PotentialMap : public DynamicStrategy {
public:
	PotentialMap(std::map<std::string, std::string> config);
	virtual ~PotentialMap();

	void track(double** voltage, double t);
	void write();

private:
	int sideNodes;
	std::string resultsDir;
	double** potentialMap;

	bool transversalActFileFlag;

};

#endif /* POTENTIALMAP_H */
