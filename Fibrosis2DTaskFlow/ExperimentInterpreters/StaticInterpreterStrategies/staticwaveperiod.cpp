#include "staticwaveperiod.h"
#include "toolset.h"

#include <vector>
#include <algorithm>
#include <utility>
#include <boost/filesystem.hpp>

using std::string;
using std::vector;
using std::map;

using boost::filesystem::directory_iterator;


StaticWavePeriod::StaticWavePeriod(std::map<std::string, std::string> unitMap)
{
    fibMode          = ToolSet::stringToInt(unitMap["fibMode"], "In fibMode (StaticWavePeriod constructor)");
    sideNodes        = ToolSet::stringToInt(unitMap["sideNodes"], "In sideNodes (StaticWavePeriod constructor)");
    dr               = ToolSet::stringToDouble(unitMap["dr"], "In dr (StaticWavePeriod constructor)");
    tMax             = ToolSet::stringToDouble(unitMap["tMax"], "In tMax (StaticWavePeriod constructor)");
    rootDirPath      = unitMap["sectionName"];
    experimentsCount = ToolSet::stringToInt(unitMap["experimentsCount"], "In experimentsCount (StaticWavePeriod constructor)");
}

StaticWavePeriod::~StaticWavePeriod()
{
}

void StaticWavePeriod::compute()
{
    string fibDeg, twigsLen, distX, distY;

    double wavePeriod;

    resultsMap.clear();

    map<std::string, std::vector<double>> wavePeriodMap;

    for (directory_iterator itr(rootDirPath); itr!=directory_iterator(); ++itr){
        string filePath = rootDirPath + "/" + itr->path().filename().string();
        if (filePath.find(".txt") != string::npos)
            continue;

        if (fibMode < 4){
            twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
            fibDeg = readConfigParameter(filePath + "/config.txt", "fibDeg");
        }
        else if (fibMode == 4){
            twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
            distX    = readConfigParameter(filePath + "/config.txt", "distX");
            distY    = readConfigParameter(filePath + "/config.txt", "distY");
        }

        wavePeriod = readValueFromFile(filePath + "/wave_period.txt");

        if (fibMode < 4){
            wavePeriodMap[twigsLen + " " + fibDeg].push_back(wavePeriod);
        }
        else if (fibMode == 4){
            wavePeriodMap[twigsLen + " " + distX + " " + distY].push_back(wavePeriod);
        }
    }

    for (auto &elem : wavePeriodMap){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }

    writeResults(rootDirPath + "/WavePeriod.txt");
}
