#ifndef VELOCITYDETECTOR_H
#define VELOCITYDETECTOR_H

#include <vector>
#include <utility> 

#include "toolset.h"

class VelocityDetector
{
private:
    int sideNodes;
    double dr;
    std::vector<std::vector<double>> targetVec;
    WAVE_DIRECTION waveDirection;
    
public:
    VelocityDetector(int sideNodes, double dr);
    void setTargetMesh(double** array); // will be converted to vector
    void setTargetMesh(std::vector<std::vector<double>> vec);
    void setSpatialStep(double dr);
    void setWaveDirection(WAVE_DIRECTION waveDirection_);
    double computeVelocity(std::vector<std::pair<int, int>> frontVec, double time, double activationTime, bool arrivalTime=false);
    
};

#endif 
