#include "fibrosismatrix.h"

#include <vector>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/algorithm/string.hpp>
#include <random>
#include <iostream>
 
using std::random_device;
using std::string;
using std::map;
using std::vector;
using std::ofstream;
using std::endl;
using std::fill;


FibrosisMatrix::FibrosisMatrix()
{
    random_device rd;
    int_gen = new boost::mt19937(rd());
    fib_range = new boost::uniform_01<boost::mt19937>(*int_gen);
}

FibrosisMatrix::~FibrosisMatrix()
{
    delete int_gen;
    delete fib_range;
}

int FibrosisMatrix::createAndWriteFibrMatrix(map <string, string> &config)
/*
 * Fibrosis types:
 * 0 - points. 
 * 1 - twigs (with specific size). Twigs oriented in the same direction.
 * 2 - points with twigs (50% with 50%).
 */
{
    // check if matrix already exists (for transversal activation)
    // if == 1 - put the same matrix and exit. 
    // Transversal activation needed in case of anisotropy computing.
    if (ToolSet::stringToInt(config["TransvActFile"], "In TransvActFile")){
        matrixVec.push_back(matrixVec[ToolSet::stringToInt(config["sameFibMatrix"], "In sameFibMatrix")]);
        return 0;
    }

    int n                 = ToolSet::stringToInt(config["sideNodes"], "In sideNodes");
    int fibMode           = ToolSet::stringToInt(config["fibMode"], "In fibMode");
    int twigsLen          = ToolSet::stringToInt(config["twigsLen"], "In twigsLen");
    double fibrPercentage;
    int distX, distY;

    if (fibMode < 4){
        fibrPercentage = ToolSet::stringToDouble(config["fibDeg"], "In fibDeg");
    }
    else if (fibMode == 4){
        distX = ToolSet::stringToInt(config["distX"], "In distX");
        distY = ToolSet::stringToInt(config["distY"], "In distY");
    }
    
    matrix.clear();
    vector<int> column;
    column.resize(n);
    fill(column.begin(), column.end(), 0);
    for (int i = 0; i < n; ++i){
        matrix.push_back(column);
    }
   
    if (fibMode == 0 || fibMode == 1){
        createBricksPattern(n, twigsLen, fibrPercentage);
    }
    else if (fibMode == 2){
        createBricksMixedPattern(n, twigsLen, fibrPercentage);
    }
    else if (fibMode == 3){
        createBlocksPattern(n, twigsLen, fibrPercentage);
    }
    else if (fibMode == 4){
        createEquidistPattern(n, twigsLen, distX, distY);
    }

    if (writeFibrosisMatrix(config, n) || writeFibrosisArea(config, n)){
        return 1;
    }
    
    matrixVec.push_back(matrix);

    return 0;
}

void FibrosisMatrix::createBricksPattern(int n, int twigsLen, double fibrPercentage)
{    
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if ((*fib_range)() < fibrPercentage && !matrix[i][j])
            {
                matrix[i][j] = 1;
                for (int k = 0; k < twigsLen; k++){
                    if (j+k < n) 
                        matrix[i][j+k] = 1;
                }
            }
}

void FibrosisMatrix::createBricksMixedPattern(int n, int twigsLen, double fibrPercentage)
{    
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if ((*fib_range)() < fibrPercentage && !matrix[i][j])
            {
                matrix[i][j] = 1;
                if ((*fib_range)() <= 0.5)
                {
                    for (int k = 0; k < twigsLen; k++){
                        if (j+k < n) 
                            matrix[i][j+k] = 1;
                    }
                }
            }
}

void FibrosisMatrix::createBlocksPattern(int n, int twigsLen, double fibrPercentage)
{    
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j += twigsLen)
            if ((*fib_range)() < fibrPercentage)
            {
                matrix[i][j] = 1;
                for (int k = 0; k < twigsLen; k++){
                    if (j+k < n) 
                        matrix[i][j+k] = 1;
                }
            }
}

void FibrosisMatrix::createEquidistPattern(int n, int twigsLen, int distX, int distY)
{
    for (int i = 0; i < n; i += distY){
        for (int j = 0; j < n; j += twigsLen + distX){
            for (int k = j; k < j + twigsLen; ++k){
                matrix[i][k] = 1; 
            }
        }
    }

}

int FibrosisMatrix::writeFibrosisMatrix(map <string, string> &config, int n)
/*
 * Write file with the fibrosis matrix. File will be placed in the task directory.
 */
{
    ofstream outstr(config["dirPath"] + "/fibrosis.txt");
    if (outstr)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
                outstr << matrix[i][j] << " ";
            outstr << endl;
        }

        return 0;
    }
    else
    {
        std::cerr << "Can't write a fibrosis matrix to file" << std::endl;

        return 1;
    }
}

int FibrosisMatrix::writeFibrosisArea(map <string, string> &config, int n)
{
    int fibrosisPointsCount = 0;
    
    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            if (matrix[i][j]){
                fibrosisPointsCount++;
            }
        }
    }
    
    ofstream fibAreaFile(config["dirPath"] + "/fibrosis_area.txt");
    if (fibAreaFile){
        fibAreaFile << ((double)fibrosisPointsCount/(n*n));
        fibAreaFile.close();

        return 0;
    }
    else{
        std::cerr << "Can't write a fibrosis area percent" << std::endl;

        return 1;
    }
}

std::vector<std::vector<std::vector<int>>> FibrosisMatrix::getFibrosisMatrixVec()
/*
 * Return 2d fibrosis matrix as a one-dim std::vector
 */
{
    return matrixVec;
}
