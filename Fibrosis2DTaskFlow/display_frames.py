import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import matplotlib.image as mpimg
import sys
import os

# Arg 1 - frames dir

frames_dir_name = sys.argv[1] 

frames_list = {}
frames_names = []

step = 0

frames_ls = os.listdir(frames_dir_name)
frames_ls.sort(key=lambda x: int(x.split(".")[0]))

for frame in frames_ls:
    if ".txt" in frame:
        if step == 1:
            frames_names.append(frame)
            frames_list[frame] = np.loadtxt(frames_dir_name + frame)
            step = 0
        else:
            step += 1

fig = plt.figure()
ax = fig.add_subplot(111)

def change_frame(key):
    ax.cla()
    ax.imshow(frames_list[key], vmin = -180, vmax = 40)
    ax.set_title(key)

class Index(object):
    ind = 0

    def next(self, event):
        self.ind += 1
        if self.ind >= len(frames_names):
            self.ind -= 1
            return
        key = frames_names[self.ind]
        change_frame(key)
        plt.draw()

    def prev(self, event):
        self.ind -= 1
        if self.ind < 0:
            self.ind += 1
            return
        key = frames_names[self.ind]
        change_frame(key)
        plt.draw()

callback = Index()
axprev = plt.axes([0.7, 0.02, 0.1, 0.075])
axnext = plt.axes([0.81, 0.02, 0.1, 0.075])
bnext = Button(axnext, 'Next')
bnext.on_clicked(callback.next)
bprev = Button(axprev, 'Previous')
bprev.on_clicked(callback.prev)

key = frames_names[0]
change_frame(key)

plt.show()
