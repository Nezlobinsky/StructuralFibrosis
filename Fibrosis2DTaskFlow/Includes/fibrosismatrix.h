#ifndef FIBROSISMATRIX_H
#define FIBROSISMATRIX_H

#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_01.hpp>

#include "toolset.h"


class FibrosisMatrix
/*
 * Construct fibrosis matrix with uniform distribution.
 */
{ 
private:
    
    std::vector<std::vector<int>> matrix;
    std::vector<std::vector<std::vector<int>>> matrixVec;
    
    boost::mt19937 *int_gen;
    boost::uniform_01<boost::mt19937> *fib_range;
  
    void createBricksPattern(int n, int twigsLen, double fibrPercentage);
    void createBricksMixedPattern(int n, int twigsLen, double fibrPercentage);
    void createBlocksPattern(int n, int twigsLen, double fibrPercentage);
    void createEquidistPattern(int n, int twigsLen, int distX, int distY);

    int writeFibrosisMatrix(std::map <std::string, std::string> &config, int n);
    int writeFibrosisArea(std::map <std::string, std::string> &config, int n);
    
public:
    FibrosisMatrix();
    ~FibrosisMatrix();
    
    int createAndWriteFibrMatrix(std::map <std::string, std::string> &config);
    
    std::vector<std::vector<std::vector<int>>> getFibrosisMatrixVec();
};

#endif
