#include "cvrestitution.h"
#include "wavedirectiondetector.h"
#include "toolset.h"

#include <iostream>
#include <fstream>

using std::make_pair;
using std::pair;
using std::vector;
using std::ofstream;
using std::endl;
using std::string;


CVRestitution::CVRestitution(std::map<string, string> config)
{
    APDFlag = false;
    uThreshold = -75.;
    firstStimulus = true;  // to skip first stimulus
    
    detectorIndex = 0;

    direction = LINE_DIRECTION::NO; 

    timeActSeries = ToolSet::splitToDouble(config["sMstimSeries"], ":");
    dr = ToolSet::stringToDouble(config["dr"], "In dr (CVRestitution constructor)");
    resultsDir = config["dirPath"];

    sideNodes = ToolSet::stringToInt(config["sideNodes"], "In sideNodes (CVRestitution constructor)");

    WaveDirectionDetector waveDirection;
	waveDirection.setStimCoordinates(config["sMArea"]);

    if (waveDirection.define2DStimulusDirection() == WAVE_DIRECTION::POSITIVE_Y ||
    	waveDirection.define2DStimulusDirection() == WAVE_DIRECTION::NEGATIVE_Y){
        direction = LINE_DIRECTION::PARALLEL_TO_X;
    }
    else{
        direction = LINE_DIRECTION::PARALLEL_TO_Y;
    }
}

void CVRestitution::track(double** voltage, double t)
{
    double uDetector;
    int lineLevelIndex = sideNodes/2;
    if (firstStimulus){
        // define which point one the line can be active (not fibrosis or not blocked)
        for (int i = 2; i < sideNodes-2; ++i){
            if (direction == LINE_DIRECTION::PARALLEL_TO_Y){
                uDetector = voltage[i][lineLevelIndex];
            }
            else{
                uDetector = voltage[lineLevelIndex][i];
            }
            if (uDetector > uThreshold){
                detectorIndex = i;
                APDFlag = true;
                firstStimulus = false;
            }
        }
    } 
    else{
         uDetector = direction == LINE_DIRECTION::PARALLEL_TO_Y ?
        		                  voltage[detectorIndex][lineLevelIndex] :
								  voltage[lineLevelIndex][detectorIndex];
         if (APDFlag){ 
            if (uDetector < uThreshold){
                DIstart = t;
                APDFlag = false;
            }
        }
        else{
            if (uDetector >= uThreshold){
                APDFlag = true;

                double velocity = (lineLevelIndex*dr)/(t - timeActSeries[0]); // velocityDetector->computeCV(frontVec, time,
                timeActSeries.erase(timeActSeries.begin());
                pair<double, double> CVRestPair;
                CVRestPair.first = velocity;
                CVRestPair.second = t - DIstart;
                cvRestitutionContainer.push_back(CVRestPair);
            }
        }
    }
}

void CVRestitution::write()
{
    string fileName = "/CVRestitution.txt";
    ofstream cvRestitutionFile(resultsDir + fileName);
    for (auto& elem : cvRestitutionContainer){
        cvRestitutionFile << elem.first << " " << elem.second << endl;
    }
    cvRestitutionFile.close();
}
