import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt


matplotlib.rc("font", **{"size" : 14})

x,y = np.meshgrid(np.arange(0, 100, 0.25), np.arange(0, 100, 0.25))
c = np.loadtxt(sys.argv[1])

plt.figure(figsize=(5,5))
plt.pcolormesh(x, y, c)
# plt.clim(20-0.5, 27-0.5)
plt.show()
