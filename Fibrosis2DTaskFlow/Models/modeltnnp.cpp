#include "modeltnnp.h"

#include <boost/algorithm/string.hpp>

#include <cmath>
#include <iostream>
#include <algorithm>
#include <omp.h> 

using boost::starts_with;

using std::sort;

ModelTNNP2D::ModelTNNP2D(std::map<std::string, std::string>& exp_config)
{
    setCalcParameters(exp_config);
}

void ModelTNNP2D::allocateVariablesMemory()
{
    allocateMemVariable(u);
    allocateMemVariable(uNext);
    allocateMemVariable(Cai);
    allocateMemVariable(CaSR);
    allocateMemVariable(CaSS);
    allocateMemVariable(Nai);
    allocateMemVariable(Ki);
    allocateMemVariable(M_);
    allocateMemVariable(H_);
    allocateMemVariable(J_);
    allocateMemVariable(Xr1);
    allocateMemVariable(Xr2);
    allocateMemVariable(Xs);
    allocateMemVariable(R_);
    allocateMemVariable(S_);
    allocateMemVariable(D_);
    allocateMemVariable(F_);
    allocateMemVariable(F2_);
    allocateMemVariable(FCass);
    allocateMemVariable(RR);
    allocateMemVariable(OO);
}

void ModelTNNP2D::freeVariablesMemory()
{
    freeMemVariable(u);
    freeMemVariable(uNext);
    freeMemVariable(Cai);
    freeMemVariable(CaSR);
    freeMemVariable(CaSS);
    freeMemVariable(Nai);
    freeMemVariable(Ki);
    freeMemVariable(M_);
    freeMemVariable(H_);
    freeMemVariable(J_);
    freeMemVariable(Xr1);
    freeMemVariable(Xr2);
    freeMemVariable(Xs);
    freeMemVariable(R_);
    freeMemVariable(S_);
    freeMemVariable(D_);
    freeMemVariable(F_);
    freeMemVariable(F2_);
    freeMemVariable(FCass);
    freeMemVariable(RR);
    freeMemVariable(OO);
    
    freeMemFibrosisMatrix();
}

void ModelTNNP2D::setVariablesInitState()
{
    setVariableToValue(u, -86.);
    setVariableToValue(uNext, -86.);
    setVariableToValue(Cai, 0.00007);
    setVariableToValue(CaSR, 1.3);
    setVariableToValue(CaSS, .00007);
    setVariableToValue(Nai, 7.67);
    setVariableToValue(Ki, 138.3);
    setVariableToValue(M_, 0.);
    setVariableToValue(H_, 0.75);
    setVariableToValue(J_, 0.75);
    setVariableToValue(Xr1, 0.);
    setVariableToValue(Xr2, 1.);
    setVariableToValue(Xs, 0.);
    setVariableToValue(R_, 0.);
    setVariableToValue(S_, 1.);
    setVariableToValue(D_, 0.);
    setVariableToValue(F_, 1.);
    setVariableToValue(F2_, 1.);
    setVariableToValue(FCass, 1.);
    setVariableToValue(RR, 1.);
    setVariableToValue(OO, 0.);
}

void ModelTNNP2D::loadVariablesInitState()
{   
    loadInitStateVariable("U.txt", u);
    loadInitStateVariable("Cai.txt", Cai);
    loadInitStateVariable("CaSR.txt", CaSR);
    loadInitStateVariable("CaSS.txt", CaSS);
    loadInitStateVariable("Nai.txt", Nai);
    loadInitStateVariable("Ki.txt", Ki);
    loadInitStateVariable("M_.txt", M_);
    loadInitStateVariable("H_.txt", H_);
    loadInitStateVariable("J_.txt", J_);
    loadInitStateVariable("Xr1.txt", Xr1);
    loadInitStateVariable("Xr2.txt", Xr2);
    loadInitStateVariable("Xs.txt", Xs);
    loadInitStateVariable("R_.txt", R_);
    loadInitStateVariable("S_.txt", S_);
    loadInitStateVariable("D_.txt", D_);
    loadInitStateVariable("F_.txt", F_);
    loadInitStateVariable("F2_.txt", F2_);
    loadInitStateVariable("FCass.txt", FCass);
    loadInitStateVariable("RR.txt", RR);
    loadInitStateVariable("OO.txt", OO);

    loadInitStateFibrosisMatrix("FibrosisMatrix.txt");
}

void ModelTNNP2D::saveVariablesState()
{   
    saveStateVariable("U.txt", u);
    saveStateVariable("Cai.txt", Cai);
    saveStateVariable("CaSR.txt", CaSR);
    saveStateVariable("CaSS.txt", CaSS);
    saveStateVariable("Nai.txt", Nai);
    saveStateVariable("Ki.txt", Ki);
    saveStateVariable("M_.txt", M_);
    saveStateVariable("H_.txt", H_);
    saveStateVariable("J_.txt", J_);
    saveStateVariable("Xr1.txt", Xr1);
    saveStateVariable("Xr2.txt", Xr2);
    saveStateVariable("Xs.txt", Xs);
    saveStateVariable("R_.txt", R_);
    saveStateVariable("S_.txt", S_);
    saveStateVariable("D_.txt", D_);
    saveStateVariable("F_.txt", F_);
    saveStateVariable("F2_.txt", F2_);
    saveStateVariable("FCass.txt", FCass);
    saveStateVariable("RR.txt", RR);
    saveStateVariable("OO.txt", OO);

    saveStateFibrosisMatrix("FibrosisMatrix.txt");
}

void ModelTNNP2D::computeI()
{
    for (int i = 0; i < sideNodes; ++i)
        for (int j = 0; j < sideNodes; ++j){
            if (IStimArea[i][j] && IStimDuration > 0.){
                uNext[i][j] += dt*IStimValue; // for the stimulation by current
            }
        }
}

void ModelTNNP2D::rewriteU()
{
    for (int i = 0; i < sideNodes; ++i)
        for (int j = 0; j < sideNodes; ++j)
            u[i][j] = uNext[i][j];
}

void ModelTNNP2D::computeLaplace()
{    
    // Five-points scheme:
    
    #pragma omp parallel for num_threads(computationalThreads)
    for (int i = 0; i < sideNodes; ++i)
    {
        for (int j = 0; j < sideNodes; ++j)
        { 
            double dudx2;
            double dudy2;
            double leftUx, leftUy, rightUx, rightUy;
            int fi, fj; // the correct fibrosis indices
    
            fi = i+fictiveLayer;
            fj = j+fictiveLayer;
            
            if (fibrosis[fi][fj])
            {
                uNext[i][j] = -180.0;
                continue;
            }
            
            if (fibrosis[fi-1][fj]){
                if (fibrosis[fi+1][fj])
                    leftUx = u[i][j];
                else
                    leftUx = u[i+1][j];
            }
            else
                leftUx = u[i-1][j];
            
            if (fibrosis[fi][fj-1]){
                if (fibrosis[fi][fj+1])
                    leftUy = u[i][j];
                else
                    leftUy = u[i][j+1];
            }
            else
                leftUy = u[i][j-1];
            
            if (fibrosis[fi+1][fj]){
                if (fibrosis[fi-1][fj])
                    rightUx = u[i][j];
                else
                    rightUx = u[i-1][j];
            }
            else
                rightUx = u[i+1][j];
            
            if (fibrosis[fi][fj+1]){
                if (fibrosis[fi][fj-1])
                    rightUy = u[i][j];
                else
                    rightUy = u[i][j-1];
            }
            else
                rightUy = u[i][j+1];
            
            dudx2 = (leftUx - 2*u[i][j] + rightUx)/(dr*dr);
            dudy2 = (leftUy - 2*u[i][j] + rightUy)/(dr*dr);
            uNext[i][j] = dt*D_along*(dudx2 + dudy2) + u[i][j];
        }
    }
}

void ModelTNNP2D::compute()
{
    t = 0;
    allocateVariablesMemory();
    if (stimMode){
        allocateIStimArea();
    }
    setVariablesInitState();
    loadVariablesInitState(); // if state directory does not exist, loading will be stopped without the errors  
       
    activate();
        
    while (t < tMax)
    {
        activate();
        
        computeLaplace();
        computeCellsState();
        if (IStimDuration > 0.){
            computeI();
            IStimDuration -= dt;
        }
        
        rewriteU();

        dynamicInterpreter->interpret(t, u, dt);

        t += dt;
    }
    
    dynamicInterpreter->writeResults(u);
    
    if (saveStateFlag)
        saveVariablesState();
    
    freeVariablesMemory();
}

void ModelTNNP2D::computeCellsState()
/*
    Code belongs to Kirsten Ten Tusscher 
*/

{  
    #pragma omp parallel for num_threads(computationalThreads)
    for (int i = 0; i < sideNodes; ++i)
    {
        for (int j = 0; j < sideNodes; ++j)
        {
            double IKr;
            double IKs;
            double IK1;
            double Ito;
            double INa;
            double IbNa;
            double ICaL;
            double IbCa;
            double INaCa;
            double IpCa;
            double IpK;
            double INaK;
            double Irel;
            double Ileak;
            double Iup;
            double Ixfer;
            double k1;
            double k2;
            double kCaSR;

            double dNai;
            double dKi;
            double dCai;
            double dCaSR;
            double dCaSS;
            double dRR;

            double Ek;
            double Ena;
            double Eks;
            double Eca;
            double CaCSQN;
            double bjsr;
            double cjsr;
            double CaSSBuf;
            double bcss;
            double ccss;
            double CaBuf;
            double bc;
            double cc;
            double Ak1;
            double Bk1;
            double rec_iK1;
            double rec_ipK;
            double rec_iNaK;
            double AM;
            double BM;
            double AH_1;
            double BH_1;
            double AH_2;
            double BH_2;
            double AJ_1;
            double BJ_1;
            double AJ_2;
            double BJ_2;
            double M_INF;
            double H_INF;
            double J_INF;
            double TAU_M;
            double TAU_H;
            double TAU_J;
            double axr1;
            double bxr1;
            double axr2;
            double bxr2;
            double Xr1_INF;
            double Xr2_INF;
            double TAU_Xr1;
            double TAU_Xr2;
            double Axs;
            double Bxs;
            double Xs_INF;
            double TAU_Xs;
            double R_INF;
            double TAU_R;
            double S_INF;
            double TAU_S;
            double Ad;
            double Bd;
            double Cd;
            double Af;
            double Bf;
            double Cf;
            double Af2;
            double Bf2;
            double Cf2;
            double TAU_D;
            double D_INF;
            double TAU_F;
            double F_INF;
            double TAU_F2;
            double F2_INF;
            double TAU_FCaSS;
            double FCaSS_INF;

            double inverseVcF2=1/(2*Vc*F);
            double inverseVcF=1./(Vc*F);
            double inversevssF2=1/(2*Vss*F);
            
            
            if (fibrosis[i+fictiveLayer][j+fictiveLayer])
                continue;
 
            //Needed to compute currents
            Ek=RTONF*(log((Ko/Ki[i][j])));
            Ena=RTONF*(log((Nao/Nai[i][j])));
            Eks=RTONF*(log((Ko+pKNa*Nao)/(Ki[i][j]+pKNa*Nai[i][j])));
            Eca=0.5*RTONF*(log((Cao/Cai[i][j])));
            Ak1=0.1/(1.+exp(0.06*(u[i][j]-Ek-200)));
            Bk1=(3.*exp(0.0002*(u[i][j]-Ek+100))+
                exp(0.1*(u[i][j]-Ek-10)))/(1.+exp(-0.5*(u[i][j]-Ek)));
            rec_iK1=Ak1/(Ak1+Bk1);
            rec_iNaK=(1./(1.+0.1245*exp(-0.1*u[i][j]*F/(R*T))+0.0353*exp(-u[i][j]*F/(R*T))));
            rec_ipK=1./(1.+exp((25-u[i][j])/5.98));

            //Compute currents
            INa=GNa*M_[i][j]*M_[i][j]*M_[i][j]*H_[i][j]*J_[i][j]*(u[i][j]-Ena);
            ICaL=GCaL*D_[i][j]*F_[i][j]*F2_[i][j]*FCass[i][j]*4*(u[i][j]-15)*(F*F/(R*T))*
            (0.25*exp(2*(u[i][j]-15)*F/(R*T))*CaSS[i][j]-Cao)/(exp(2*(u[i][j]-15)*F/(R*T))-1.);
            Ito=Gto*R_[i][j]*S_[i][j]*(u[i][j]-Ek);
            IKr=Gkr*sqrt(Ko/5.4)*Xr1[i][j]*Xr2[i][j]*(u[i][j]-Ek);
            IKs=Gks*Xs[i][j]*Xs[i][j]*(u[i][j]-Eks);
            IK1=GK1*rec_iK1*(u[i][j]-Ek);
            INaCa=knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*
            (1./(1+ksat*exp((n_-1)*u[i][j]*F/(R*T))))*
            (exp(n_*u[i][j]*F/(R*T))*Nai[i][j]*Nai[i][j]*Nai[i][j]*Cao-
            exp((n_-1)*u[i][j]*F/(R*T))*Nao*Nao*Nao*Cai[i][j]*2.5);
            INaK=knak*(Ko/(Ko+KmK))*(Nai[i][j]/(Nai[i][j]+KmNa))*rec_iNaK;
            IpCa=GpCa*Cai[i][j]/(KpCa+Cai[i][j]);
            IpK=GpK*rec_ipK*(u[i][j]-Ek);
            IbNa=GbNa*(u[i][j]-Ena);
            IbCa=GbCa*(u[i][j]-Eca);

            //Determine total current
            uNext[i][j] += -dt*(IKr    +
            IKs   +
            IK1   +
            Ito   +
            INa   +
            IbNa  +
            ICaL  +
            IbCa  +
            INaK  +
            INaCa +
            IpCa  +
            IpK);
            
            //update concentrations    
            kCaSR=maxsr-((maxsr-minsr)/(1+(EC/CaSR[i][j])*(EC/CaSR[i][j]))); 
            k1=k1_/kCaSR;
            k2=k2_*kCaSR;
            dRR=k4*(1-RR[i][j])-k2*CaSS[i][j]*RR[i][j];
            RR[i][j]+=dt*dRR;
            OO[i][j]=k1*CaSS[i][j]*CaSS[i][j]*RR[i][j]/(k3+k1*CaSS[i][j]*CaSS[i][j]);

            Irel=Vrel*OO[i][j]*(CaSR[i][j]-CaSS[i][j]);
            Ileak=Vleak*(CaSR[i][j]-Cai[i][j]);
            Iup=Vmaxup/(1.+((Kup*Kup)/(Cai[i][j]*Cai[i][j])));
            Ixfer=Vxfer*(CaSS[i][j]-Cai[i][j]);

            CaCSQN=Bufsr*CaSR[i][j]/(CaSR[i][j]+Kbufsr);
            dCaSR=dt*(Iup-Irel-Ileak);
            bjsr=Bufsr-CaCSQN-dCaSR-CaSR[i][j]+Kbufsr;
            cjsr=Kbufsr*(CaCSQN+dCaSR+CaSR[i][j]);
            CaSR[i][j]=(sqrt(bjsr*bjsr+4*cjsr)-bjsr)/2;
        
            CaSSBuf=Bufss*CaSS[i][j]/(CaSS[i][j]+Kbufss);
            dCaSS=dt*(-Ixfer*(Vc/Vss)+Irel*(Vsr/Vss)+(-ICaL*inversevssF2*CAPACITANCE));
            bcss=Bufss-CaSSBuf-dCaSS-CaSS[i][j]+Kbufss;
            ccss=Kbufss*(CaSSBuf+dCaSS+CaSS[i][j]);
            CaSS[i][j]=(sqrt(bcss*bcss+4*ccss)-bcss)/2;

            CaBuf=Bufc*Cai[i][j]/(Cai[i][j]+Kbufc);
            dCai=dt*((-(IbCa+IpCa-2*INaCa)*inverseVcF2*CAPACITANCE)-(Iup-Ileak)*(Vsr/Vc)+Ixfer);
            bc=Bufc-CaBuf-dCai-Cai[i][j]+Kbufc;
            cc=Kbufc*(CaBuf+dCai+Cai[i][j]);
            Cai[i][j]=(sqrt(bc*bc+4*cc)-bc)/2;
                
            dNai=-(INa+IbNa+3*INaK+3*INaCa)*inverseVcF*CAPACITANCE;
            Nai[i][j]+=dt*dNai;
            
            dKi=-(IK1+Ito+IKr+IKs-2*INaK+IpK)*inverseVcF*CAPACITANCE;
            Ki[i][j]+=dt*dKi;

            //compute steady state values and time constants 
            AM=1./(1.+exp((-60.-u[i][j])/5.));
            BM=0.1/(1.+exp((u[i][j]+35.)/5.))+0.10/(1.+exp((u[i][j]-50.)/200.));
            TAU_M=AM*BM;
            M_INF=1./((1.+exp((-56.86-u[i][j])/9.03))*(1.+exp((-56.86-u[i][j])/9.03)));
            if (u[i][j]>=-40.)
            {
                AH_1=0.; 
                BH_1=(0.77/(0.13*(1.+exp(-(u[i][j]+10.66)/11.1))));
                TAU_H= 1.0/(AH_1+BH_1);
            }
            else
            {
                AH_2=(0.057*exp(-(u[i][j]+80.)/6.8));
                BH_2=(2.7*exp(0.079*u[i][j])+(3.1e5)*exp(0.3485*u[i][j]));
                TAU_H=1.0/(AH_2+BH_2);
            }
            H_INF=1./((1.+exp((u[i][j]+71.55)/7.43))*(1.+exp((u[i][j]+71.55)/7.43)));
            if(u[i][j]>=-40.)
            {
                AJ_1=0.;      
                BJ_1=(0.6*exp((0.057)*u[i][j])/(1.+exp(-0.1*(u[i][j]+32.))));
                TAU_J= 1.0/(AJ_1+BJ_1);
            }
            else
            {
                AJ_2=(((-2.5428e4)*exp(0.2444*u[i][j])-(6.948e-6)*
                        exp(-0.04391*u[i][j]))*(u[i][j]+37.78)/
                    (1.+exp(0.311*(u[i][j]+79.23))));    
                BJ_2=(0.02424*exp(-0.01052*u[i][j])/(1.+exp(-0.1378*(u[i][j]+40.14))));
                TAU_J= 1.0/(AJ_2+BJ_2);
            }
            J_INF=H_INF;

            Xr1_INF=1./(1.+exp((-26.-u[i][j])/7.));
            axr1=450./(1.+exp((-45.-u[i][j])/10.));
            bxr1=6./(1.+exp((u[i][j]-(-30.))/11.5));
            TAU_Xr1=axr1*bxr1;
            Xr2_INF=1./(1.+exp((u[i][j]-(-88.))/24.));
            axr2=3./(1.+exp((-60.-u[i][j])/20.));
            bxr2=1.12/(1.+exp((u[i][j]-60.)/20.));
            TAU_Xr2=axr2*bxr2;

            Xs_INF=1./(1.+exp((-5.-u[i][j])/14.));
            Axs=(1400./(sqrt(1.+exp((5.-u[i][j])/6))));
            Bxs=(1./(1.+exp((u[i][j]-35.)/15.)));
            TAU_Xs=Axs*Bxs+80;
            
        #ifdef EPI
            R_INF=1./(1.+exp((20-u[i][j])/6.));
            S_INF=1./(1.+exp((u[i][j]+20)/5.));
            TAU_R=9.5*exp(-(u[i][j]+40.)*(u[i][j]+40.)/1800.)+0.8;
            TAU_S=85.*exp(-(u[i][j]+45.)*(u[i][j]+45.)/320.)+5./(1.+exp((u[i][j]-20.)/5.))+3.;
        #endif
        #ifdef ENDO
            R_INF=1./(1.+exp((20-u[i][j])/6.));
            S_INF=1./(1.+exp((u[i][j]+28)/5.));
            TAU_R=9.5*exp(-(u[i][j]+40.)*(u[i][j]+40.)/1800.)+0.8;
            TAU_S=1000.*exp(-(u[i][j]+67)*(u[i][j]+67)/1000.)+8.;
        #endif
        #ifdef MCELL
            R_INF=1./(1.+exp((20-u[i][j])/6.));
            S_INF=1./(1.+exp((u[i][j]+20)/5.));
            TAU_R=9.5*exp(-(u[i][j]+40.)*(u[i][j]+40.)/1800.)+0.8;
            TAU_S=85.*exp(-(u[i][j]+45.)*(u[i][j]+45.)/320.)+5./(1.+exp((u[i][j]-20.)/5.))+3.;
        #endif

            D_INF=1./(1.+exp((-8-u[i][j])/7.5));
            Ad=1.4/(1.+exp((-35-u[i][j])/13))+0.25;
            Bd=1.4/(1.+exp((u[i][j]+5)/5));
            Cd=1./(1.+exp((50-u[i][j])/20));
            TAU_D=Ad*Bd+Cd;
            F_INF=1./(1.+exp((u[i][j]+20)/7));
            Af=1102.5*exp(-(u[i][j]+27)*(u[i][j]+27)/225);
            Bf=200./(1+exp((13-u[i][j])/10.));
            Cf=(180./(1+exp((u[i][j]+30)/10)))+20;
            TAU_F=Af+Bf+Cf;
            F2_INF=0.67/(1.+exp((u[i][j]+35)/7))+0.33;
            Af2=600*exp(-(u[i][j]+25)*(u[i][j]+25)/170);
            Bf2=31/(1.+exp((25-u[i][j])/10));
            Cf2=16/(1.+exp((u[i][j]+30)/10));
            TAU_F2=Af2+Bf2+Cf2;
            FCaSS_INF=0.6/(1+(CaSS[i][j]/0.05)*(CaSS[i][j]/0.05))+0.4;
            TAU_FCaSS=80./(1+(CaSS[i][j]/0.05)*(CaSS[i][j]/0.05))+2.;
        
            //Update gates
            M_[i][j] = M_INF-(M_INF-M_[i][j])*exp(-dt/TAU_M);
            H_[i][j] = H_INF-(H_INF-H_[i][j])*exp(-dt/TAU_H);
            J_[i][j] = J_INF-(J_INF-J_[i][j])*exp(-dt/TAU_J);
            Xr1[i][j] = Xr1_INF-(Xr1_INF-Xr1[i][j])*exp(-dt/TAU_Xr1);
            Xr2[i][j] = Xr2_INF-(Xr2_INF-Xr2[i][j])*exp(-dt/TAU_Xr2);
            Xs[i][j] = Xs_INF-(Xs_INF-Xs[i][j])*exp(-dt/TAU_Xs);
            S_[i][j]= S_INF-(S_INF-S_[i][j])*exp(-dt/TAU_S);
            R_[i][j]= R_INF-(R_INF-R_[i][j])*exp(-dt/TAU_R);
            D_[i][j] = D_INF-(D_INF-D_[i][j])*exp(-dt/TAU_D); 
            F_[i][j] =F_INF-(F_INF-F_[i][j])*exp(-dt/TAU_F); 
            F2_[i][j] =F2_INF-(F2_INF-F2_[i][j])*exp(-dt/TAU_F2); 
            FCass[i][j] =FCaSS_INF-(FCaSS_INF-FCass[i][j])*exp(-dt/TAU_FCaSS);     
        }
    }
        
}
