#include "modelinterface2d.h"
#include "toolset.h"

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <algorithm> 
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

using std::string;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::endl;
using std::sort;
using std::nothrow;

using boost::split;
using boost::starts_with;
using boost::is_any_of;
using boost::token_compress_on;
using boost::filesystem::exists;
using boost::filesystem::create_directory;


ModelInterface2D::ModelInterface2D()
{
    //default parameters:
    sideNodes = 150;
    IStimValue = 0.;
    IStimDuration = 0.;
    s2Mode = 0;
    sMMode = 0;
    currentMode = 0;
    s2Time = 0;
    computationalThreads = 1;
    
    experimentDir = "";
    
    saveStateFlag = 0;
    
    initStateDir = "";

}

ModelInterface2D::~ModelInterface2D(){}

void ModelInterface2D::setCalcParameters(std::map<std::string, std::string>& exp_config)
{
    // Set the basic parameters:
    sideNodes = ToolSet::stringToInt(exp_config["sideNodes"], "In sideNodes");
    tMax      = ToolSet::stringToDouble(exp_config["tMax"], "In tMax");
    dt        = ToolSet::stringToDouble(exp_config["dt"], "In dt");
    dr        = ToolSet::stringToDouble(exp_config["dr"], "In dr");
    stimMode  = ToolSet::stringToInt(exp_config["stimMode"], "In stimMode");
    
    // Find the init state if exist:
    auto stateDirIt = exp_config.find("initStateFile"); 
    if (stateDirIt != exp_config.end())
        setInitStateDir(exp_config["initStateFile"]);

    // Set the experiment protocol mode:
    int mode  = ToolSet::stringToInt(exp_config["protocolMode"], "In protocolMode");
    if (mode == 1){
        s2Mode = 1;
    } 
    else if (mode == 2){
        s2Mode = 1;
        sMMode = 1;
    }

    if (s2Mode){
        s2Time = ToolSet::stringToDouble(exp_config["s2Time"], "In exp_config");
    }
    
    // Set the stimuli series in case of sM experiment protocol:
    if (sMMode){
        sMStimSeriesVector = ToolSet::splitToDouble(exp_config["sMstimSeries"], ":");
        // and sort them to hold the order (from smallest to largest)
        sort(sMStimSeriesVector.begin(), sMStimSeriesVector.end());
    }

    // stimul configuration:
    s1StimVector = ToolSet::splitToDouble(exp_config["s1Area"], ":");
    // if transversalAct:
    if (ToolSet::stringToInt(exp_config["TransvActFile"], "In TransvActFile")){
        double tmp = s1StimVector[0];
        s1StimVector[0] = s1StimVector[2];
        s1StimVector[2] = tmp;
        
        tmp = s1StimVector[1];
        s1StimVector[1] = s1StimVector[3];
        s1StimVector[3] = tmp;
        
    }
    if (!stimMode) 
        s1StimVector.push_back(ToolSet::stringToDouble(exp_config["s1ValU"], "In s1ValU"));
    else{
        s1StimVector.push_back(ToolSet::stringToDouble(exp_config["s1ValI"], "In s1ValI"));
        s1StimVector.push_back(ToolSet::stringToDouble(exp_config["s1Dur"], "In s1Dur"));
    }
    
    if (s2Mode){
        s2StimVector = ToolSet::splitToDouble(exp_config["s2Area"], ":");
        if (!stimMode)  
            s2StimVector.push_back(ToolSet::stringToDouble(exp_config["s2ValU"], "In s2ValU"));
        else{
            s2StimVector.push_back(ToolSet::stringToDouble(exp_config["s2ValI"], "In s2ValI"));
            s2StimVector.push_back(ToolSet::stringToDouble(exp_config["s2Dur"], "In s2Dur"));
        }
    }

    if (sMMode){
        sMStimVector = ToolSet::splitToDouble(exp_config["sMArea"], ":");

        if (!stimMode)  
            sMStimVector.push_back(ToolSet::stringToDouble(exp_config["sMValU"], "In sMValU"));
        else{
            sMStimVector.push_back(ToolSet::stringToDouble(exp_config["sMValI"], "In sMValI"));
            sMStimVector.push_back(ToolSet::stringToDouble(exp_config["sMDur"], "In sMDur"));
        }
    }
    
    experimentDir = exp_config["dirPath"];
    saveStateFlag = ToolSet::stringToInt(exp_config["saveState"], "In saveState"); 
}

void ModelInterface2D::allocateIStimArea()
/*
 * To activate mesh part by a current, boolean matrix is used.
 * Element with a true value means that this element must be activated by a current 
 * with a certain duration.
 */
{
    IStimArea = new(nothrow) bool*[sideNodes];
    if (!IStimArea){
        std::cout << "Bad allocation" << endl;
        exit(1);
    }
    for (int i = 0; i < sideNodes; ++i){
         IStimArea[i] = new(nothrow) bool[sideNodes];
        if (!IStimArea[i]){
            std::cout << "Bad allocation" << endl;
            exit(1);
        }
    }
}

void ModelInterface2D::freeIStimArea()
{
    for (int i = 0; i < sideNodes; ++i){
         delete[] IStimArea[i];
    }
    delete[] IStimArea;
}

void ModelInterface2D::allocateMemFibrosisMatrix()
/*
 For the fibrosis matrix only. Uses additional number of layers (fictive).
 */
{
    fibrosis = new(nothrow) int*[sideNodes + 2*fictiveLayer];
    if (!fibrosis){
        std::cout << "Bad allocation" << endl;
        exit(1);
    }
    for (int i = 0; i < sideNodes + 2*fictiveLayer; ++i){
        fibrosis[i] = new(nothrow) int[sideNodes + 2*fictiveLayer];
        if (!fibrosis[i]){
            std::cout << "Bad allocation" << endl;
            exit(1);
        }
    }
}

void ModelInterface2D::freeMemFibrosisMatrix()
{
    for (int i = 0; i < sideNodes + 2*fictiveLayer; ++i){
         delete[] fibrosis[i];
    }
    delete[] fibrosis;
}

void ModelInterface2D::allocateMemVariable(double **&variable)
{
    variable = new(nothrow) double*[sideNodes];
    if (!variable){
        std::cout << "Bad allocation" << endl;
        exit(1);
    }
    for (int i = 0; i < sideNodes; ++i){
        variable[i] = new(nothrow) double[sideNodes];
        if (!variable){
            std::cout << "Bad allocation" << endl;
            exit(1);
        }
    }
}

void ModelInterface2D::freeMemVariable(double **&variable)
{
    for (int i = 0; i < sideNodes; ++i){
         delete[] variable[i];
    }
    delete[] variable;
}

void ModelInterface2D::setVariableToValue(double **variable, double value)
{
    for(int i = 0; i < sideNodes; ++i)
        for(int j = 0; j < sideNodes; ++j)
            variable[i][j] = value;
}

void ModelInterface2D::setFibrosisMatrix(std::vector<std::vector<int>> fibrosisVec)
/*
 * Construct fibrosis matrix from the one-dim std::vector
 */
{
     allocateMemFibrosisMatrix();
     
     for (int i = 0; i < sideNodes; ++i){
         for (int j = 0; j < sideNodes; ++j){
             fibrosis[i+fictiveLayer][j+fictiveLayer] = fibrosisVec[i][j];
         }
     }
     
     // Fill the boundary layers with non-cell points:
     for (int i = 0; i < sideNodes+fictiveLayer; ++i){
         fibrosis[i][0] = 1;
         fibrosis[0][i] = 1;
         fibrosis[i][sideNodes+2*fictiveLayer-1] = 1;
         fibrosis[sideNodes+2*fictiveLayer-1][i] = 1;
    }
}

void ModelInterface2D::setInitStateDir(std::string stateDir)
/*
 * Set initial state directory which should contain all variables for the mesh 
 * with the same dimension (as .txt files).
 */
{
    initStateDir = stateDir;
}

void ModelInterface2D::loadInitStateVariable(std::string variableName, double** variable)
/*
 * Load variable state (variable values for all elements on the mesh). 
 * Variable should has double type.
 */
{
    if (initStateDir.empty())
        return;
        
    ifstream initStateFile(initStateDir + "/" + variableName);
    if (!initStateFile){
        std::cout << "Can't open this file:" << initStateDir + "/" + variableName <<  std::endl;
        std::cout << "State file will be ignored:" << initStateDir + "/" + variableName <<  std::endl;
        return; 
    }
    
    int i = 0;
    for (string line; getline(initStateFile, line);){
        vector<string> lineVecStr;
        split(lineVecStr, line, is_any_of(" "), token_compress_on);
        for (int j = 0; j < sideNodes; ++j){
            variable[i][j] = ToolSet::stringToDouble(lineVecStr[j]);
        }
        i++;        
    }
    initStateFile.close();
}

void ModelInterface2D::loadInitStateFibrosisMatrix(std::string variableName)
/*
 * Load fibrosis matrix state. 
 */
{
    if (initStateDir.empty())
        return;
        
    ifstream initStateFile(initStateDir + "/" + variableName);
    if (!initStateFile){
        std::cout << "Fibrosis state file will be ignored:" << initStateDir + "/" + variableName <<  std::endl;
        return; 
    }
    
    int i = 0;
    for (string line; getline(initStateFile, line);){
        for (int j = 0; j < line.length(); ++j){
            fibrosis[i+fictiveLayer][j+fictiveLayer] = line[j] - '0'; // convertion to int 
        }
        i++;        
    }
    initStateFile.close();
    
     // Fill the boundary layers with non-cell points:
     for (int i = 0; i < sideNodes+fictiveLayer; ++i){
         fibrosis[i][0] = 1;
         fibrosis[0][i] = 1;
         fibrosis[i][sideNodes+2*fictiveLayer-1] = 1;
         fibrosis[sideNodes+2*fictiveLayer-1][i] = 1;
    }
}

void ModelInterface2D::saveStateVariable(std::string variableName, double** variable)
/*
 * Create VariablesState directory (if not exist) and write variable as a .txt file
 * Variable should has double type
 */
{
    std::string saveStateDir = experimentDir + "/VariablesState";
    if(!(exists(saveStateDir))){
        if (create_directory(saveStateDir)){
        }
    }
    
    ofstream stateFile(saveStateDir + "/" + variableName);
    if (!stateFile){
        std::cout << "Can't open this file:" << saveStateDir + "/" + variableName <<  std::endl;
        return; 
    }
    
    for (int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j){
            stateFile << variable[i][j] << " ";
        }
        stateFile << endl;
    }
    stateFile.close();
}

void ModelInterface2D::saveStateFibrosisMatrix(std::string variableName)
/*
 * Create VariablesState directory (if not exist) and write fibrosis matrix as a .txt file
 */
{
    std::string saveStateDir = experimentDir + "/VariablesState";
    if(!(exists(saveStateDir))){
        if (create_directory(saveStateDir)){
        }
    }
    
    ofstream stateFile(saveStateDir + "/" + variableName);
    if (!stateFile){
        std::cout << "Can't open this file:" << saveStateDir + "/" + variableName <<  std::endl;
        return; 
    }
    
    for (int i = fictiveLayer; i < sideNodes + fictiveLayer; ++i){
        for (int j = fictiveLayer; j < sideNodes + fictiveLayer; ++j){
            stateFile << fibrosis[i][j];
        }
        stateFile << endl;
    }
    stateFile.close();
}

void ModelInterface2D::setSideNodes(int n)
{
    sideNodes = n;
}

void ModelInterface2D::setDt(double dt_)
{
    dt = dt_;
}

void ModelInterface2D::setDr(double dr_)
{
    dr = dr_;
}

void ModelInterface2D::setTMax(double tMax_)
{
    tMax = tMax_;
}

void ModelInterface2D::setComputationalThreads(int computationalThreads_)
{
    computationalThreads = computationalThreads_;
}

void ModelInterface2D::activateByU(int startx,int endx, int starty, int endy, double value)
{
    // bounds cheking:
    startx = (startx > sideNodes) ? 0 : startx;
    endx = (endx > sideNodes) ? sideNodes : endx;
    starty = (starty > sideNodes) ? 0 : starty;
    endy = (endy > sideNodes) ? sideNodes : endy;
    
    for(int i = startx; i < endx; ++i)
        for (int j = starty; j < endy; ++j)
            u[i][j] = value;
    
    IStimValue = 0.;
    IStimDuration = 0.;
}

void ModelInterface2D::activateByI(int startx,int endx, int starty, int endy, double value, double duration)
/*
 * Stimulation for current is described as a bool array (size the same as the mesh size),
 * where false - should not stimulate, true - should be stimulated.
 */
{
    // bounds cheking:
    startx = (startx > sideNodes) ? 0 : startx;
    endx = (endx > sideNodes) ? sideNodes : endx;
    starty = (starty > sideNodes) ? 0 : starty;
    endy = (endy > sideNodes) ? sideNodes : endy;

    for(int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j){
            if (i > startx && i < endx && j > starty && j < endy)
                IStimArea[i][j] = true;
            else
                IStimArea[i][j] = false;
        }
    }
    IStimValue = value;
    IStimDuration = duration;
}

void ModelInterface2D::activate()
/*
 * Activate the part of the mesh with specified stim protocol.
 * 
 * There are 3 experiment protocols:
 * 0 (s1)   - just a single stimulus. 
 * 1 (s1s2) - two stimuli (from the different parts of the mesh) nedeed (usually) to launch spiral wave.
 * 2 (sM)   - a set of stimuli from the certain part of the mesh. 
 * 
 * And 2 stim modes:
 * 0 - by U (just a value in mV (or model units in case of phenomenological models)). 
 * 1 - by I. Then nedeed to also specify the duration of stimulus. 
 * 
 * Note that every next experiment protocol incorporates previous experiment protocols. 
 */
{    
    // currentMode - points to the current experiment protocol (from 0 to 2) 
    // S1 stimul:

    if (stimMode == 0 && !currentMode){ // rectang_U
        activateByU(static_cast<int> (s1StimVector[0]),
                    static_cast<int> (s1StimVector[1]),
                    static_cast<int> (s1StimVector[2]),
                    static_cast<int> (s1StimVector[3]),
                    s1StimVector[4]);
    currentMode++;
    }
    else if (stimMode == 1 && !currentMode){ //rectang_I
        activateByI(static_cast<int> (s1StimVector[0]),
                    static_cast<int> (s1StimVector[1]),
                    static_cast<int> (s1StimVector[2]),
                    static_cast<int> (s1StimVector[3]),
                    s1StimVector[4],
                    s1StimVector[5]);
    currentMode++;
    }
    
    // S2 stimul:
    if (stimMode == 0 && s2Mode && currentMode == 1 && t >= s2Time){ // rectang_U
        activateByU(static_cast<int> (s2StimVector[0]),
                    static_cast<int> (s2StimVector[1]),
                    static_cast<int> (s2StimVector[2]),
                    static_cast<int> (s2StimVector[3]),
                    s2StimVector[4]);
    currentMode++;
    }
    else if (stimMode == 1 && s2Mode && currentMode == 1 && t >= s2Time){ //rectang_I
        activateByI(static_cast<int> (s2StimVector[0]),
                    static_cast<int> (s2StimVector[1]),
                    static_cast<int> (s2StimVector[2]),
                    static_cast<int> (s2StimVector[3]),
                    s2StimVector[4],
                    s2StimVector[5]);
    currentMode++;
    }

    if (sMStimSeriesVector.size() == 0)
        return;

    // S3 stimul (special protocol):
    // activates after the time contains in sMStimSeriesVector
    if (stimMode == 0 && sMMode && currentMode == 2 && t >= sMStimSeriesVector[0]){ // rectang_U
        activateByU(static_cast<int> (sMStimVector[0]),
                    static_cast<int> (sMStimVector[1]),
                    static_cast<int> (sMStimVector[2]),
                    static_cast<int> (sMStimVector[3]),
                    sMStimVector[4]);
        sMStimSeriesVector.erase(sMStimSeriesVector.begin());
    }
    else if (stimMode == 1 && sMMode && currentMode == 2 && t >= sMStimSeriesVector[0]){ //rectang_I
        activateByI(static_cast<int> (sMStimVector[0]),
                    static_cast<int> (sMStimVector[1]),
                    static_cast<int> (sMStimVector[2]),
                    static_cast<int> (sMStimVector[3]),
                    sMStimVector[4],
                    sMStimVector[5]);
        sMStimSeriesVector.erase(sMStimSeriesVector.begin());
    }
}

double ModelInterface2D::getU(int i, int j) const
/*
 * Return action potential value in i,j element 
 */
{
    return u[i][j];
}
double ModelInterface2D::getTime() const
/*
 * Return current time value
 */
{
    return t;
}

void ModelInterface2D::setDynamicInterpreter(DynamicInterpreter* interpreter)
{
    dynamicInterpreter = interpreter;
}
