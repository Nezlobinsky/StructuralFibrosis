#ifndef FRONTCOMPLEXITY_H
#define FRONTCOMPLEXITY_H

#include <map>
#include <string>
#include <vector>
#include <utility>
#include "staticstrategy.h"

class FrontComplexity : public StaticStrategy
{
public:
	FrontComplexity(std::map<std::string, std::string> unitMap);
	virtual ~FrontComplexity();

	void compute();

private:
	double computeFrontLength(std::vector<std::pair<int, int>> frontVec);
	void writeWaveFront(std::string fileName, std::vector<std::pair<int, int>> frontVec);
	void writerWaveFrontLength(std::string fileName, double length);
};

#endif /* FRONTCOMPLEXITY_H */
