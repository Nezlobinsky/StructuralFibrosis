#include <fstream>
#include <iostream>
#include <vector>
#include <boost/regex.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>

#include "configprocessor.h"
#include "toolset.h"

using boost::split;
using boost::starts_with;
using boost::regex;
using boost::regex_match;
using boost::cmatch;

using std::string;
using std::map;
using std::remove;
using std::vector;
using std::ifstream;
using std::getline;
using std::cerr;
using std::endl;


ConfigProcessor::ConfigProcessor(std::vector<std::map<std::string, std::string>>& unitsVec) : unitsVector(unitsVec)
{ 
}

int ConfigProcessor::Read(const std::string fileName)
{
    ifstream instr(fileName);
    string prev_line = "";
    bool empty_start = true; // if there are empty lines at the beginning of unit file
    
    std::map<std::string, std::string> unit;

    if (instr)
    {
        for (string line; getline(instr, line);)
        {
            
            if (line.empty())
            {
                if (!prev_line.empty() && !empty_start){
                    /* 
                     * means that previous block end and we need to
                     * push formed data to vectors 
                     */
                    unitsVector.push_back(unit);
                    unit.clear();
                }
                prev_line = line;
                continue;
            }
            else if (starts_with(line, "#"))
                // skip comments:
                continue;
            else
                empty_start = false;
            
            ToolSet::removeSpaces(line);
            
            vector<string> lineVec = ToolSet::splitToString(line, "=");

            string paramName = lineVec[0];
            string value;
            if (starts_with(paramName, "{")){
                value = lineVec[0];
            }            
            else{
                value = lineVec[1];   
            }
            unit[paramName] = value;
            
            prev_line = line;
        }
        if (instr.eof() && !prev_line.empty()){
           /* 
            * means end of file and we need to
            * push formed data to vectors 
            */
            unitsVector.push_back(unit);
            unit.clear();
        }

        if (instr.bad()) {
            cerr << "Unrecoverable read error" << endl;
            return 2;
        }
    }
    else{
    	return 1;
    }
    return  0;
}

