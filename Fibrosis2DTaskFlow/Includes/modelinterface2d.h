#ifndef MODELINTERFACE_H
#define MODELINTERFACE_H

#include <string>
#include <vector>
#include <map>

#include "toolset.h"
#include "dynamicinterpreter.h"


class ModelInterface2D
/* 
 * An abstract class for all electrophysiological models. 
 * All parameters and methods general for all models are declared here. 
 * All pure methods (with () = 0) must be implemented in inherited classes. 
 */
{

protected:
    std::string experimentDir;
    
/* Calc parameters: */
    int sideNodes;
    const int fictiveLayer = 1;
    double tMax;
    double t;
    double dt;
    double dr;
    int computationalThreads;

/* Initialization: */
    void setVariableToValue(double**, double);

/* Base model variables: */
    double **u;
    int **fibrosis;
    
/* States controlling: */
    int saveStateFlag;
    std::string initStateDir;
    void loadInitStateVariable(std::string variableName, double** variable);
    void loadInitStateFibrosisMatrix(std::string variableName);
    void saveStateVariable(std::string variableName, double** variable);
    void saveStateFibrosisMatrix(std::string variableName);
    virtual void setVariablesInitState() = 0;
    virtual void loadVariablesInitState() = 0;
    virtual void saveVariablesState() = 0;

/* Stim structures and methods: */
    double IStimValue;
    double IStimDuration;
    double arrivalTimeThreshold;
    bool **IStimArea;
    int s2Mode;
    int sMMode;
    int currentMode;
    int stimMode; // by U or by I
    double s2Time;
    std::vector<double> s1StimVector;
    std::vector<double> s2StimVector;
    std::vector<double> sMStimVector;
    std::vector<double> sMStimSeriesVector;
    void activateByU(int ,int, int, int, double);
    void activateByI(int ,int, int, int, double, double);
    void activate();

/* Arrays memory allocation: */
    void allocateIStimArea();
    void freeIStimArea();
    void allocateMemFibrosisMatrix();
    void freeMemFibrosisMatrix();
    void allocateMemVariable(double**&);
    void freeMemVariable(double**&);
    virtual void allocateVariablesMemory() = 0;
    virtual void freeVariablesMemory() = 0;

    DynamicInterpreter *dynamicInterpreter;   

public:
    explicit ModelInterface2D();
    virtual ~ModelInterface2D();

/* Model an calc parameters getters: */
    double getU(int, int) const;
    double getTime() const;

/* Initialization: */
    void setCalcParameters(std::map<std::string, std::string>&);
    void setFibrosisMatrix(std::vector<std::vector<int>>);
    
/* States controlling: */
    void setInitStateDir(std::string stateDir);

/* Calc parameters setters: */
    void setSideNodes(int);
    void setDt(double);
    void setDr(double);
    void setTMax(double);
    void setComputationalThreads(int);

/* Computational methods: */
    virtual void compute() = 0;

/* Interpreter: */
    void setDynamicInterpreter(DynamicInterpreter*);

};

#endif // MODELINTERFACE_H
