#ifndef FIBROSISAREA_H
#define FIBROSISAREA_H

#include <map>
#include <string>
#include "staticstrategy.h"

class FibrosisArea : public StaticStrategy
{
public:
	FibrosisArea(std::map<std::string, std::string> unitMap);
	virtual ~FibrosisArea();

	void compute();
};

#endif /* FIBROSISAREA_H */
