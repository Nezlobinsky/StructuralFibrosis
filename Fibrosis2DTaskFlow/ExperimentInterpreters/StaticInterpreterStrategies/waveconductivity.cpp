#include "waveconductivity.h"
#include "wavefrontdetector.h"
#include "velocitydetector.h"
#include "wavedirectiondetector.h"
#include "toolset.h"

#include <vector>
#include <algorithm>
#include <utility>
#include <boost/filesystem.hpp>

using std::string;
using std::vector;
using std::map;
using std::pair;

using boost::filesystem::directory_iterator;

WaveConductivity::WaveConductivity(std::map<std::string, std::string> unitMap)
{
    fibMode          = ToolSet::stringToInt(unitMap["fibMode"], "In fibMode (WaveConductivity constructor)");
    sideNodes        = ToolSet::stringToInt(unitMap["sideNodes"], "In sideNodes (WaveConductivity constructor)");
    dr               = ToolSet::stringToDouble(unitMap["dr"], "In dr (WaveConductivity constructor)");
    tMax             = ToolSet::stringToDouble(unitMap["tMax"], "In tMax (WaveConductivity constructor)");
    rootDirPath      = unitMap["sectionName"];
    experimentsCount = ToolSet::stringToInt(unitMap["experimentsCount"], "In experimentsCount (WaveConductivity constructor)");
}

WaveConductivity::~WaveConductivity()
{
}

void WaveConductivity::compute()
// An error occurs here if there was no wave propagation throught the mesh


{
    string fibDeg, twigsLen, distX, distY, s1Area;

    double conductivity;
    WAVE_DIRECTION stimulusDirection;
    vector<pair<int, int>> frontVec;
    vector<vector<double>> arrivalTime;

    WaveFrontDetector waveFrontDetector = WaveFrontDetector(sideNodes, dr);
    VelocityDetector velocityDetector = VelocityDetector(sideNodes, dr);
    WaveDirectionDetector waveDirectionDetector = WaveDirectionDetector();

    resultsMap.clear();

    map<std::string, std::vector<double>> conductivityMap;

    for (directory_iterator itr(rootDirPath); itr!=directory_iterator(); ++itr)
        {
            string filePath = rootDirPath + "/" + itr->path().filename().string();
            if (filePath.find(".txt") != string::npos)
                continue;

            if (fibMode < 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                fibDeg = readConfigParameter(filePath + "/config.txt", "fibDeg");
            }
            else if (fibMode == 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                distX    = readConfigParameter(filePath + "/config.txt", "distX");
                distY    = readConfigParameter(filePath + "/config.txt", "distY");
            }


            s1Area = readConfigParameter(filePath + "/config.txt", "s1Area");
            arrivalTime = readArrivalTime(filePath + "/arrivalTime.txt");
            // define wave front direction:
            waveDirectionDetector.setStimCoordinates(s1Area);
            stimulusDirection = waveDirectionDetector.define2DStimulusDirection();

            // define wave front (wave bound):
            waveFrontDetector.setTargetMesh(arrivalTime);
            waveFrontDetector.setWaveDirection(stimulusDirection);
            frontVec = waveFrontDetector.findWaveFront();

            // compute wave velocity:
            velocityDetector.setTargetMesh(arrivalTime);
            velocityDetector.setWaveDirection(stimulusDirection);
            conductivity = velocityDetector.computeVelocity(frontVec, 0., tMax, true);

            if (fibMode < 4){
                conductivityMap[twigsLen + " " + fibDeg].push_back(conductivity);
            }
            else if (fibMode == 4){
                conductivityMap[twigsLen + " " + distX + " " + distY].push_back(conductivity);
            }

            arrivalTime.clear();
        }

    for (auto &elem : conductivityMap){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }

    writeResults(rootDirPath + "/Conductivity.txt");
}
