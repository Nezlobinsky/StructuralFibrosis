#ifndef WAVEFRONTDETECTOR_H
#define WAVEFRONTDETECTOR_H

#include <vector>
#include <string>
#include <tuple>

#include "toolset.h"

class WaveFrontDetector
{
private:
       
    int sideNodes;
    double dr;
    std::vector<int> stimCoordinatesVec;
    std::vector<std::vector<double>> targetVec;
    WAVE_DIRECTION waveDirection;

    void rainPoints(std::vector<std::pair<int, int>>& frontCoordinates);
    
    void sortFrontCoordinates(std::vector<std::pair<int, int>>& frontCoordinates);
    
    std::vector<std::tuple<std::pair<int, int>, std::pair<int, int>, int>> findSpecialPoints(std::vector<std::pair<int, int>> frontCoordinates);
    
    double distance(std::pair<int, int> p1, std::pair<int, int> p2);
    
    std::vector<double> normVector(std::vector<double> vec);
    
    std::vector<double> orthoVec2D(std::vector<double> vec);
    
    std::vector<double> reverseVec(std::vector<double> vec);
    
    std::pair<double, double> towardToVec(std::pair<double, double> point, std::vector<double> vec);

public:
    WaveFrontDetector(int sideNodes, double dr);
    void setTargetMesh(double** array); // will be converted to vector
    void setTargetMesh(std::vector<std::vector<double>> vec);
    void setWaveDirection(WAVE_DIRECTION waveDirection_);
    std::vector<std::pair<int, int>> findWaveFront();
    
};

#endif
