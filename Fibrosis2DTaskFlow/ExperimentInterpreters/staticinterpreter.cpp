#include "staticinterpreter.h"
#include "anisotropy.h"
#include "fibrosisarea.h"
#include "frontcomplexity.h"
#include "staticwaveperiod.h"
#include "waveconductivity.h"
#include "toolset.h"

#include <cmath>
#include <string>
#include <vector>
#include <utility> 
#include <algorithm>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using std::string;
using std::vector;
using std::map;
using std::pair;
using std::make_pair;
using std::accumulate;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::sort;

using boost::filesystem::directory_iterator;
using boost::split;
using boost::is_any_of;
using boost::starts_with;


StaticInterpreter::StaticInterpreter(std::map<std::string, std::string> unitMap)
{
    setInterpretVec(unitMap);
}

void StaticInterpreter::setInterpretVec(std::map<std::string, std::string> unitMap)
{
    for (auto& elem : unitMap){
        if (starts_with(elem.first, "{")){
            interpretModes.push_back(elem.first);
        }
    }

    for (auto& mode : interpretModes){
        if (mode == "{Conductivity}")
        	waveConductivity.setStaticStrategy(new WaveConductivity(unitMap));
        if (mode == "{Anisotropy}")
        	anisotropyContext.setStaticStrategy(new Anisotropy(unitMap)) ;
        if (mode == "{FrontComplexity}")
        	frontComplexityContext.setStaticStrategy(new FrontComplexity(unitMap)) ;
        if (mode == "{WavePeriod}")
        	staticWavePeriodContext.setStaticStrategy(new StaticWavePeriod(unitMap));
        if (mode == "{FibrosisArea}")
        	fibrosisAreaContext.setStaticStrategy(new FibrosisArea(unitMap)) ;
    }
}

void StaticInterpreter::interpret()
{  
    for (auto& mode : interpretModes){
        if (mode == "{Conductivity}")
        	waveConductivity.execute();
        if (mode == "{Anisotropy}")
        	anisotropyContext.execute();
        if (mode == "{FrontComplexity}")
        	frontComplexityContext.execute();
        if (mode == "{WavePeriod}")
        	staticWavePeriodContext.execute();
        if (mode == "{FibrosisArea}")
        	fibrosisAreaContext.execute();
    }
}
