/*
 * activationmap.h
 *
 *  Created on: Feb 27, 2018
 *      Author: collapse
 */

#ifndef ACTIVATIONMAP_H
#define ACTIVATIONMAP_H

#include <map>
#include <string>
#include "dynamicstrategy.h"


class ActivationMap : public DynamicStrategy {
public:
	ActivationMap(std::map<std::string, std::string> config);
	virtual ~ActivationMap();

	void track(double** voltage, double t);
	void write();

private:
	int sideNodes;
	int transversalActFileFlag;
	std::string resultsDir;
	double** arrivalTime;
};

#endif /* ACTIVATIONMAP_H */
