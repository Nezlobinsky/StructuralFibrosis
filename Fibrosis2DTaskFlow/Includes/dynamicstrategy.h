/*
 * DynamicStrategyInterface.h
 *
 *  Created on: Feb 27, 2018
 *      Author: collapse
 */

#ifndef DYNAMICSTRATEGY_H
#define DYNAMICSTRATEGY_H


class DynamicStrategy {
public:
	//DynamicStrategy(){};
	virtual ~DynamicStrategy(){};

	virtual void track(double** voltage, double t) = 0;

	virtual void write() = 0;
};

#endif /* DYNAMICSTRATEGY_H */
