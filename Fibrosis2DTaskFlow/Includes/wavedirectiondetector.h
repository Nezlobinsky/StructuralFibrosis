#ifndef WAVEDIRECTIONDETECTOR_H
#define WAVEDIRECTIONDETECTOR_H

#include <string>
#include <vector>

#include "toolset.h"


class WaveDirectionDetector
{
private:    
    std::vector<int> stimCoordinatesVec;

public:
    WaveDirectionDetector(){};
    void transversal();
    void setStimCoordinates(std::string str);
    WAVE_DIRECTION define2DStimulusDirection();
};

#endif
