import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

font = {'size'   : 13}

matplotlib.rc('font', **font)


area_dict = {0: [[], [], [], [], []], 
             5: [[], [], [], [], []], 
             10: [[], [], [], [], []], 
             15: [[], [], [], [], []],
             20: [[], [], [], [], []],
             25: [[], [], [], [], []],
             30: [[], [], [], [], []],
             35: [[], [], [], [], []]}
#size_dict = {"5": [[], [], [], [], []], 
#             "6": [[], [], [], [], []],
#             "7": [[], [], [], []]}


with open(sys.argv[1]) as rfile:
    for line in rfile:
        line = line.split()
        size  = int(line[0].split(".")[0])
        prob  = int(float(line[1])*100)
        value = float(line[2])
        max_v = float(line[3])
        min_v = float(line[4]) 
                
        area_dict[prob][0].append(size)
        area_dict[prob][1].append(value)
        area_dict[prob][2].append(max_v - value)
        area_dict[prob][3].append(value - min_v)

if len(sys.argv) > 2:
    with open(sys.argv[2]) as rfile:
        for line in rfile:
            line = line.split()
            size  = line[0].split(".")[0]
            value = float(line[2])*100
                
            size_dict[size][4].append(value)
    
    for key in size_dict:
        plt.errorbar(size_dict[key][4],
                     size_dict[key][1], 
                     yerr=[size_dict[key][2], size_dict[key][3]], label="size " + key)

else:
    for key in area_dict:
        plt.errorbar(area_dict[key][0],
                     area_dict[key][1], 
                     yerr=[area_dict[key][2], area_dict[key][3]], label="fibrosis " + str(key))

plt.legend(loc=0)
plt.title("Wave velocity")
plt.ylabel("Velocity (mm/ms)")
plt.xlabel("Size")
#plt.xlabel("Probability, %")
plt.grid()
plt.show()
