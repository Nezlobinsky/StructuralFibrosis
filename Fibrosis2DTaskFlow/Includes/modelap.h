#ifndef MODELAP_H
#define MODELAP_H

#include <string>
#include <map>

#include "modelinterface2d.h"
#include "toolset.h"



class ModelAP : public ModelInterface2D
{

public:
    ModelAP(std::map<std::string, std::string>& exp_config);
/* Initialization setters: */
    //void setModelParameters(std::map<std::string, double>);
    
private:
/* Initialization setters: */
    void setVariablesInitState();
    void loadVariablesInitState();
    void saveVariablesState();

/* Computational methods: */
    void compute();
    void computeLaplace();
    void computeCellsState();
    void computeI();
    void expandToScale();
    void rewriteU();

/* Model variables: */
    double **uNext; // u on the next step
    double **uScale; // u with the real (from -80 to 20 mV) units; 
    double **v;

/* Arrays memory allocation: */
    void allocateVariablesMemory();
    void freeVariablesMemory();

/* Constants */
    const double D_along = 1.;
    const double k = 8.;
    const double a = 0.1;
    const double eap = 0.01;
    const double mu1 = 0.2;
    const double mu2 = 0.3;

};

#endif 
 
