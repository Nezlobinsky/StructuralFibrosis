import os
import sys
from math import cos, sin, radians, degrees, sqrt
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt


t = 50
dr = 0.25
xc = 200
yc = 200


def read_size(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("twigsLen"):
                return int(float(line_content[1].strip("\n")))

def read_fibDeg(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("fibDeg"):
                return float(line_content[1].strip("\n"))

def findBoundForward(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = 0
        while (j < n and p_map[j][i] < -30):
            j += 1
        if j < n and p_map[j][i] >= -30:
            bound[0].append(i)
            bound[1].append(j)
    return bound

def findBoundBackward(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = n-1
        while (j > -1 and p_map[j][i] < -30):
            j -= 1
        if j > -1 and p_map[j][i] >= -30:
            bound[0].append(i)
            bound[1].append(j)
    return bound

def findBoundLeft(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = n-1
        while (j > -1 and p_map[i][j] < -30):
            j -= 1
        if j > -1 and p_map[i][j] >= -30:
            bound[0].append(i)
            bound[1].append(j)
    return bound

def findBoundRight(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = 0
        while (j > -1 and p_map[i][j] < -30):
            j += 1
        if j > -1 and p_map[i][j] >= -30:
            bound[0].append(i)
            bound[1].append(j)
    return bound

def find_velocity(coords, center):
    velocity = []
    angle = []
    x0, y0 = center
    for i in range(len(coords[0])):
        x = coords[0][i] - x0
        y = coords[1][i] - y0
        velocity.append((sqrt(x**2 + y**2)*dr)/t)
        angle.append(np.arctan2(y, x))
    return velocity, angle

def diffusion_expr(alpha, D1, D2):
    alpha = np.radians(alpha)
    base_vel = 0.75
    x = base_vel*np.cos(alpha)
    y = base_vel*np.sin(alpha)
    return np.sqrt((x*D1)**2 + (y*D2)**2)

D1_err_list = []
D2_err_list = []

fibDeg = {}
fibSize = {}

plot_d = {}
count = 0
for dir_ in os.listdir(sys.argv[1]):
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue

    fibSz = read_size(os.path.join(sys.argv[1], dir_, "config.txt"))
    if fibSz != 4: 
        continue
#    else:
#        fibSize[fibSz] = []
    
    fibDg = read_fibDeg(os.path.join(sys.argv[1], dir_, "config.txt"))
    if float(fibDg) != 0.15:
        continue
#    if fibDg in fibDeg:
#        continue
#    else:
#       fibDeg[fibDg] = []

    print dir_, fibDg, fibSz

    matr = np.loadtxt(os.path.join(sys.argv[1], dir_, "potentialMap.txt"))

    bdf = findBoundForward(matr)
    bdb = findBoundBackward(matr)
    bdl = findBoundBackward(matr)
    bdr = findBoundBackward(matr)

    side1 = find_velocity(bdf, [xc, yc])
    side2 = find_velocity(bdb, [xc, yc])
    side3 = find_velocity(bdl, [xc, yc])
    side4 = find_velocity(bdr, [xc, yc])

    velocity = side1[0] + side2[0] + side3[0] + side4[0]
    angle = side1[1] + side2[1] + side3[1] + side4[1]

    velocity = velocity[1:]
    angle = angle[1:]

    coords = zip(angle, velocity)
    min_ = degrees(min(angle))

    for i in  range(len(coords)):
        coords[i] = list(coords[i])
        coords[i][0] = degrees(coords[i][0]) - min_

    coords = np.array(sorted(coords, key=lambda el: el[0]))

    opt, cov = curve_fit(diffusion_expr, coords[:, 0], coords[:, 1], bounds=([0.1, 0.1], [1, 1]))
    D1_err_list.append(np.sqrt(np.diag(cov))[0])
    D2_err_list.append(np.sqrt(np.diag(cov))[1])

    #plt.plot(coords[:, 0], coords[:, 1], "-")
    plot_d[count] = diffusion_expr(np.arange(0, 360), *opt)
    count += 1
    #plt.title(str(opt[0]) + " " + str(opt[1]))
    # plt.xlim([-5, 5])
    # plt.ylim([-5, 5])
    #plt.savefig(os.path.join(sys.argv[1], dir_, "fit.png"))
    #plt.close()
    
    #plt.matshow(matr)
    #plt.plot(bdf[0], bdf[1], "r")
    #plt.plot(bdb[0], bdb[1], "r")
    #plt.savefig(os.path.join(sys.argv[1], dir_, "ell_map.png"))
    #plt.close()

    # with open(os.path.join(sys.argv[1], dir_, "D_ratio.txt"), "w") as fl:
    #     fl.write(str(opt[0]) + " " + str(opt[1]))
plt.title("Diffusion coefficients estimation: fibrosis area 15%, size 4")
plt.ylabel("Velocity (mm/ms)")
plt.xlabel("Angle (deg)")

mean_v_ls = []
max_v_ls = []
min_v_ls = []

for i in range(0, 360):
    vals = []
    for key in sorted(plot_d.keys()):
        vals.append(plot_d[key][i])
    mean_v_ls.append(np.mean(vals))
    if i % 45 == 0:
        max_v_ls.append(np.max(vals) - mean_v_ls[-1])
        min_v_ls.append(mean_v_ls[-1] - np.min(vals))
    else:
        max_v_ls.append(0)
        min_v_ls.append(0)

plt.errorbar(np.arange(0, 360), mean_v_ls, yerr=[max_v_ls, min_v_ls])

#plt.legend(loc=0)
plt.grid()
plt.show()
# print np.mean(D1_err_list), np.mean(D2_err_list)
