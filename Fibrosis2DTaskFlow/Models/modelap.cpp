#include "modelap.h"

#include <boost/algorithm/string.hpp>

#include <cmath>
#include <iostream>
#include <algorithm>
#include <omp.h> 

using boost::starts_with;

ModelAP::ModelAP(std::map<std::string, std::string>& exp_config)
{
    setCalcParameters(exp_config);
}

void ModelAP::allocateVariablesMemory()
{
    allocateMemVariable(u);
    allocateMemVariable(uNext);
    allocateMemVariable(uScale);
    allocateMemVariable(v);
}

void ModelAP::freeVariablesMemory()
{
    freeMemVariable(u);
    freeMemVariable(uNext);
    freeMemVariable(uScale);
    freeMemVariable(v);
    
    freeMemFibrosisMatrix();
}

void ModelAP::setVariablesInitState()
{
    setVariableToValue(u, 0.);
    setVariableToValue(uNext, 0.);
    setVariableToValue(uScale, -80.);
    setVariableToValue(v, 0.);
}

void ModelAP::loadVariablesInitState()
{   
    loadInitStateVariable("u.txt", u);
    loadInitStateVariable("v.txt", v);

    loadInitStateFibrosisMatrix("FibrosisMatrix.txt");
}

void ModelAP::saveVariablesState()
{   
    saveStateVariable("u.txt", u);
    saveStateVariable("v.txt", v);

    saveStateFibrosisMatrix("FibrosisMatrix.txt");
}

void ModelAP::computeI()
{
    for (int i = 0; i < sideNodes; ++i)
        for (int j = 0; j < sideNodes; ++j){
            if (IStimArea[i][j] && IStimDuration > 0.){
                uNext[i][j] += dt*IStimValue; // for the stimulation by current
            }
        }
}

void ModelAP::rewriteU()
{
    for (int i = 0; i < sideNodes; ++i)
        for (int j = 0; j < sideNodes; ++j)
            u[i][j] = uNext[i][j];
}

void ModelAP::expandToScale()
{
    for (int i = 0; i < sideNodes; ++i)
        for (int j = 0; j < sideNodes; ++j){
            int fi = i+fictiveLayer;
            int fj = j+fictiveLayer;
            if (fibrosis[fi][fj])
            {
                uScale[i][j] = -180.0;
                continue;
            }
            uScale[i][j] = 100*u[i][j] - 80.;
        }
}

void ModelAP::computeLaplace()
{    
    // Five-points scheme:

    #pragma omp parallel for num_threads(computationalThreads)
    for (int i = 0; i < sideNodes; ++i)
    {
        for (int j = 0; j < sideNodes; ++j)
        { 
            double dudx2;
            double dudy2;
            double leftUx, leftUy, rightUx, rightUy;
            int fi, fj; // the correct fibrosis indices
                
            fi = i+fictiveLayer;
            fj = j+fictiveLayer;
            
            if (fibrosis[fi][fj])
            {
                uNext[i][j] = -180.0;
                continue;
            }
            
            if (fibrosis[fi-1][fj]){
                if (fibrosis[fi+1][fj])
                    leftUx = u[i][j];
                else
                    leftUx = u[i+1][j];
            }
            else
                leftUx = u[i-1][j];
            
            if (fibrosis[fi][fj-1]){
                if (fibrosis[fi][fj+1])
                    leftUy = u[i][j];
                else
                    leftUy = u[i][j+1];
            }
            else
                leftUy = u[i][j-1];
            
            if (fibrosis[fi+1][fj]){
                if (fibrosis[fi-1][fj])
                    rightUx = u[i][j];
                else
                    rightUx = u[i-1][j];
            }
            else
                rightUx = u[i+1][j];
            
            if (fibrosis[fi][fj+1]){
                if (fibrosis[fi][fj-1])
                    rightUy = u[i][j];
                else
                    rightUy = u[i][j-1];
            }
            else
                rightUy = u[i][j+1];
            
            dudx2 = (leftUx - 2*u[i][j] + rightUx)/(dr*dr);
            dudy2 = (leftUy - 2*u[i][j] + rightUy)/(dr*dr);
            uNext[i][j] = dt*D_along*(dudx2 + dudy2) + u[i][j];
        }
    }
}

void ModelAP::compute()
{
    t = 0;
    allocateVariablesMemory();
    if (stimMode){
        allocateIStimArea();
    }
    setVariablesInitState();
    loadVariablesInitState(); // if state directory does not exist, loading will be stopped without the errors  
      
    activate();
    
    while (t < tMax)
    {
        activate();
        
        computeLaplace();
        computeCellsState();
        if (IStimDuration > 0.){
            computeI();
            IStimDuration -= dt;
        }

        rewriteU();
        expandToScale();

        dynamicInterpreter->interpret(t, uScale, dt);

        t += dt;
    }
    
    dynamicInterpreter->writeResults(uScale);
    
    if (saveStateFlag)
        saveVariablesState();
    
    freeVariablesMemory();
}

void ModelAP::computeCellsState()
{
    #pragma omp parallel for num_threads(computationalThreads)
    for (int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j){
            if (fibrosis[i+fictiveLayer][j+fictiveLayer])
                continue;
            
            uNext[i][j] += dt*(-k*uNext[i][j]*(uNext[i][j]-a)*(uNext[i][j]-1.0)-uNext[i][j]*v[i][j]);
            v[i][j] -= dt*(eap + (mu1*v[i][j])/(mu2 + uNext[i][j]))*(v[i][j] + k*uNext[i][j]*(uNext[i][j]-a-1.0));
        }
    }
}

