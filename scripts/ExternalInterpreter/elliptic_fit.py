from os.path import join as ojoin
import os
import sys
import copy
import numpy as np
from numpy.linalg import eig, inv
import matplotlib.pyplot as plt


dir_name_list = []

fib_deg_list = []
twg_len_list = []

a_ellip_dict = {}
b_ellip_dict = {}


def merge_structures(dict_, list1, list2):
    for key1 in list1:
        dict_[key1] = {}
        for key2 in list2:
            dict_[key1][key2] = []

def read_config(file_name):
    global fib_deg_list, twg_len_list
    with open(file_name) as fl:
        for line in fl:
            line = line.strip().replace(" ", "")
            if line.startswith("sectionName"):
                dir_name_list.append(line.split("=")[1])
            elif line.startswith("fibDegSeries"):
                fib_deg_list = line.split("=")[1].split(":")
            elif line.startswith("twigsLenSeries"):
                twg_len_list = line.split("=")[1].split(":")

def read_parameter(file_name, parameter):
    with open(file_name) as fl:
        for line in fl:
            line = line.strip().replace(" ", "")
            if line.startswith(parameter):
                return line.split("=")[1]

def fitEllipse(x, y):
    x = x[:,np.newaxis]
    y = y[:,np.newaxis]
    D =  np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    S = np.dot(D.T,D)
    C = np.zeros([6,6])
    C[0,2] = C[2,0] = 2; C[1,1] = -1
    E, V =  eig(np.dot(inv(S), C))
    n = np.argmax(np.abs(E))
    a = V[:,n]
    return a

def ellipse_axis_length(a):
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    down1=(b*b-a*c)*( (c-a)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    down2=(b*b-a*c)*( (a-c)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    res1=np.sqrt(up/down1)
    res2=np.sqrt(up/down2)
    return np.array([res1, res2])

def findBoundForward(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = 0
        while (j < n and p_map[i][j] < -30):
            j += 1
        if j < n and p_map[i][j] >= -30:
            bound[0].append(j)
            bound[1].append(i)
    return bound

def findBoundBackward(p_map):
    bound = [[], []]
    n = p_map.shape[0]
    for i in range(0, n):
        j = n-1
        while (j > -1 and p_map[i][j] < -30):
            j -= 1
        if j > -1 and p_map[i][j] >= -30:
            bound[0].append(j)
            bound[1].append(i)
    return bound


config_path = sys.argv[1]
exp_set_path = os.path.split(config_path)[0]

read_config(config_path)

merge_structures(a_ellip_dict, fib_deg_list, twg_len_list)
merge_structures(b_ellip_dict, fib_deg_list, twg_len_list)

for dir_name in dir_name_list:
    for exp in os.listdir(ojoin(exp_set_path, dir_name)):
        pmap = np.loadtxt(ojoin(exp_set_path, dir_name, exp, "potentialMap.txt"))
        bdf = findBoundForward(pmap)
        bdb = findBoundBackward(pmap)
        fit = fitEllipse(np.array(bdf[0] + bdb[0]), np.array(bdf[1] + bdb[1]))
        axes = ellipse_axis_length(fit)
        a, b = axes

        fib_deg = str(float(read_parameter(ojoin(exp_set_path, dir_name, exp, "config.txt"),
                                 "fibDeg")))
        twg_len = str(int(float(read_parameter(ojoin(exp_set_path, dir_name, exp, "config.txt"),
                                 "twigsLen"))))

        a_ellip_dict[fib_deg][twg_len].append(a)
        b_ellip_dict[fib_deg][twg_len].append(b)

with open("EllipticFit_a.txt", "w") as fl:
    for key1 in a_ellip_dict:
        for key2 in a_ellip_dict[key1]:
            fl.write(key1 + " " + key2 + " " + str(np.mean(a_ellip_dict[key1][key2])) + "\n")

with open("EllipticFit_b.txt", "w") as fl:
    for key1 in b_ellip_dict:
        for key2 in b_ellip_dict[key1]:
            fl.write(key1 + " " + key2 + " " + str(np.mean(b_ellip_dict[key1][key2])) + "\n")
