#include <vector>
#include <iostream>
#include <boost/algorithm/string.hpp>

#include "toolset.h"

using std::string;
using std::vector;
using std::stoi;
using std::stod;
using std::invalid_argument;
using std::cerr;
using std::endl;

using boost::split;
using boost::is_any_of;


void ToolSet::removeSpaces(string& str)
/**
 * Remove all-size spaces from the input string
 */
{
    str.erase(remove(str.begin(), str.end(), ' '), str.end());
}

vector<string> ToolSet::splitToString(string targetString, string separator)
/**
 * Divide a string into separate elements-substrings
 */
{
    vector<string> stringVec;
    split(stringVec, targetString, is_any_of(separator));
    
    return stringVec;
}


vector<double> ToolSet::splitToDouble(string targetString, string separator)
/**
 * Divide a string into separate elements-double values
 */
{
    vector<string> stringVec;
    split(stringVec, targetString, is_any_of(separator));
    
    vector<double> doubleVec;
    for (auto& elem : stringVec){
        doubleVec.push_back(stringToDouble(elem, "In " + targetString));
    }
    
    return doubleVec;
}


vector<int> ToolSet::splitToInt(string targetString, string separator)
/**
 * Divide a string into separate elements-integer values
 */
{
    vector<string> stringVec;
    split(stringVec, targetString, is_any_of(separator));
    
    vector<int> intVec;
    for (auto& elem : stringVec){
        intVec.push_back(stringToInt(elem, "In " + targetString));
    }
    
    return intVec;
}

int ToolSet::stringToInt(std::string targetString, std::string additionalMsg)
/**
 * More accurate report about invalid argument for the stoi
 */
{
    int result = 0;

    try{
        result = stoi(targetString);
    }
    catch(const invalid_argument& inv){
        cerr << "Invalid argument in stoi: " << "'" << targetString << "'" << " " << additionalMsg << endl;
    }
    
    return result;
}
double ToolSet::stringToDouble(std::string targetString, std::string additionalMsg)
/**
 * More accurate report about invalid argument for the stod
 */
{
    double result = 0.;

    try{
        result = stod(targetString);
    }
    catch(const invalid_argument& inv){
        cerr << "Invalid argument in stod: " << "'" << targetString << "'" << " " << additionalMsg << endl;
    }
    
    return result;
}
