#include "dynamicinterpreter.h"
#include "activationmap.h"
#include "potentialmap.h"
#include "potentialmapanimation.h"
#include "waveperiod.h"
#include "cvrestitution.h"
#include "toolset.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <cmath>

using std::ofstream;
using std::endl;
using std::string;
using std::to_string;
using std::vector;
using std::map;
using std::pair;
using std::make_pair;

using boost::filesystem::exists;
using boost::filesystem::create_directory;
using boost::starts_with;
using boost::split;
using boost::is_any_of;


DynamicInterpreter::DynamicInterpreter(map<string, string> config)
{   
    periodWriterFlag = false;
    frameWriterFlag = false;
    cvRestitutionFlag = false;    

    setInterpreterCalcParameters(config);
}

DynamicInterpreter::~DynamicInterpreter()
{
}

void DynamicInterpreter::setInterpreterCalcParameters(map<string, string> config)
{
    vector<string> interpretModes; 
    for (auto& elem : config){
        if (starts_with(elem.first, "{")){
            interpretModes.push_back(elem.first);
        }
    }
    setInterpreterModes(interpretModes, config);
}

void DynamicInterpreter::setInterpreterModes(vector<string> interpretModes, map<string, string> config)
{
	activationMapContext.setDynamicStrategy(new ActivationMap(config));
	potentialMapContext.setDynamicStrategy(new PotentialMap(config));

    for (auto& mode : interpretModes){
        if (mode == "{WavePeriod}"){ // wave period writer
        	wavePeriodContext.setDynamicStrategy(new WavePeriod(config));
            periodWriterFlag = true;
        }
        if (mode == "{Animation}"){ // animation writer
        	potentialMapAnimationContext.setDynamicStrategy(new PotentialMapAnimation(config));
            frameWriterFlag = true;
        }
        if (mode == "{CVRestitution}"){ // CV restitution
        	cvRestitutionContext.setDynamicStrategy(new CVRestitution(config));
            cvRestitutionFlag = true;
        }
    }
}

void DynamicInterpreter::interpret(double t, double** voltage, double dt)
{
	activationMapContext.execute(voltage, t);
    
    if (frameWriterFlag){
    	potentialMapAnimationContext.execute(voltage, t);
    	potentialMapAnimationContext.write();
    }
    
    if (periodWriterFlag){
    	wavePeriodContext.execute(voltage, t);
    }
    
    if(cvRestitutionFlag){
    	cvRestitutionContext.execute(voltage, t);
    }
}

void DynamicInterpreter::writeResults(double** voltage)
{
	potentialMapContext.execute(voltage);
	potentialMapContext.write();

	activationMapContext.write();
    
    if (periodWriterFlag){
    	wavePeriodContext.write();
    }
    
    if (cvRestitutionFlag){
    	cvRestitutionContext.write();
    }
}
