#ifndef STATICSTRATEGY_H
#define STATICSTRATEGY_H

#include "toolset.h"

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <boost/algorithm/string.hpp>


class StaticStrategy {
public:
	//StaticStrategy(){};
	virtual ~StaticStrategy(){};

	virtual void compute() = 0;

protected:
    int fibMode;
    int sideNodes;
    double dr;
    double tMax;
    std::string rootDirPath;
    int experimentsCount;

	std::map<std::string, std::vector<double>> resultsMap;

	void writeResults(std::string fileName)
	{
	    std::ofstream resFile(fileName);

	    for (auto &elem : resultsMap){
	        resFile << elem.first << " " << elem.second[0] << " "
	            << elem.second[1] << " " << elem.second[2] << std::endl;
	    }
	    resFile.close();
	}

	std::vector<std::vector<double>> readArrivalTime(std::string arrivalTimeFile)
	{
	    std::vector<std::vector<double>> arrivalTime;

	    std::ifstream arrFile(arrivalTimeFile);
	    if (!arrFile){
	        std::cout << "Can't open this file:" << arrivalTimeFile <<  std::endl;
	        exit(1);
	    }
	    int i = 0;
	    arrivalTime.resize(sideNodes);
	    for (std::string line; getline(arrFile, line);){
	        std::vector<std::string> lineVecStr;
	        boost::split(lineVecStr, line, boost::is_any_of(" "));
	        for (int j = 0; j < lineVecStr.size(); ++j){
	            if (j == sideNodes)
	                break;
	            arrivalTime[i].resize(sideNodes);
	            arrivalTime[i][j] = ToolSet::stringToDouble(lineVecStr[j]);
	        }
	        i++;
	    }
	    arrFile.close();

	    return arrivalTime;
	}

	double readValueFromFile(std::string fileName)
	{
	    double number = -1.;

	    std::ifstream targetFile(fileName);
	    for (std::string line; std::getline(targetFile, line);){
	        number = ToolSet::stringToDouble(line);
	    }
	    targetFile.close();

	    return number;
	}

	std::string readConfigParameter(std::string configFile, std::string parameterName)
	{
	    std::ifstream confFile(configFile);
	    for (std::string line; std::getline(confFile, line);){
	        std::vector<std::string> lineVecStr;
	        boost::split(lineVecStr, line, boost::is_any_of("="));
	        if (boost::starts_with(lineVecStr[0], parameterName)){
	            confFile.close();
	            ToolSet::removeSpaces(lineVecStr[1]);
	            return lineVecStr[1];
	        }
	    }
	    confFile.close();
	}
};

#endif /* STATICSTRATEGY_H */
