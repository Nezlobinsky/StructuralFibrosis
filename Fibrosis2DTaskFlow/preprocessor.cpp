#include <fstream>
#include <iostream>
#include <vector>
#include <boost/regex.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>

#include "preprocessor.h"
#include "toolset.h"

using boost::split;
using boost::regex;
using boost::regex_match;
using boost::cmatch;

using std::string;
using std::map;
using std::remove;
using std::vector;
using std::ifstream;
using std::getline;
using std::cerr;
using std::endl;
using std::to_string;


Preprocessor::Preprocessor(std::vector<std::map<std::string, std::string>>& experimentsVec): experimentsVector(experimentsVec) 
{ 
    minimalParametersVec = {"sectionName", 
                            "modelType",
                            "experimentsCount",
                            "sideNodes",
                            "tMax",
                            "dt",
                            "dr",
                            "stimMode",
                            "protocolMode",
                            "fibMode",
                            "fibDegSeries",
                            "saveState"};
}

void Preprocessor::process(std::vector<std::map<std::string, std::string>>& unitsVec)
{
    for (auto& unit : unitsVec){
        checkUnitCorrectness(unit);
        int experimentsCount = ToolSet::stringToInt(unit["experimentsCount"], "In experimentsCount");
        for (int i = 0; i < experimentsCount; ++i){
            buildExperimentsVector(unit);
        }
    }
}

void Preprocessor::buildExperimentsVector(std::map<std::string, std::string>& unit)
{
    int fibMode = ToolSet::stringToInt(unit["fibMode"], "In fibMode");
    
    if (fibMode < 4){
    	buildRandomPatternVector(unit, fibMode);
    }
    else if (fibMode == 4){
    	buildEquidistPatternVector(unit, fibMode);
    }
}

void Preprocessor::buildRandomPatternVector(std::map<std::string, std::string>& unit, int fibMode)
{
    vector<double> fibDegVec = ToolSet::splitToDouble(unit["fibDegSeries"], ":");
    vector<double> twigsLenVec;
    if (!fibMode)
        twigsLenVec.push_back(1); // fibrosis length = 1 (point)
    else if (fibMode)
        twigsLenVec = ToolSet::splitToDouble(unit["twigsLenSeries"], ":");

    for (auto& twigsLen : twigsLenVec){
        for (auto& fibDeg : fibDegVec){
            unit["twigsLen"] = to_string(twigsLen);
            unit["fibDeg"] = to_string(fibDeg);
            unit["dirPath"] = unit["sectionName"] + "/exp" + to_string(experimentsVector.size());
            unit["TransvActFile"] = "0";
            experimentsVector.push_back(unit);
            if (ToolSet::stringToInt(unit["transvAct"], "In transvAct")){
                unit["TransvActFile"] = "1";
                // to account for transversal activation (same matrix):
                unit["sameFibMatrix"] = to_string(experimentsVector.size()-1);
                experimentsVector.push_back(unit);
            }
        }
    }
}

void Preprocessor::buildEquidistPatternVector(std::map<std::string, std::string>& unit, int fibMode)
{
    vector<double> twigsLenVec = ToolSet::splitToDouble(unit["twigsLenSeries"], ":");
    vector<double> distXVec = ToolSet::splitToDouble(unit["distXSeries"], ":");
    vector<double> distYVec = ToolSet::splitToDouble(unit["distYSeries"], ":");

    for (auto& twigsLen : twigsLenVec){
        for (auto& distX : distXVec){
            for (auto& distY : distYVec){
                unit["twigsLen"] = to_string(twigsLen);
                unit["distX"] = to_string(distX);
                unit["distY"] = to_string(distY);
                unit["dirPath"] = unit["sectionName"] + "/exp" + to_string(experimentsVector.size());
                unit["TransvActFile"] = "0";
                experimentsVector.push_back(unit);
                if (ToolSet::stringToInt(unit["transvAct"], "In transvAct")){
                    unit["TransvActFile"] = "1";
                    // to account for transversal activation (same matrix):
                    unit["sameFibMatrix"] = to_string(experimentsVector.size()-1);
                    experimentsVector.push_back(unit);
                }
            }
        }
    }
}

void Preprocessor::checkUnitCorrectness(std::map<std::string, std::string> unit)
{
    // check all minimal parameters:
//     for (auto& elem : minimalParametersVec){
//         check(findParameter(unit, elem), elem);
//     }
//     
//     if (!ToolSet::stringToInt(unit["stimMode"], "In stimMode")){
//         check(findParameter(unit, "s1ValU"), "s1ValU");
//         if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 1)
//             check(findParameter(unit, "s2ValU"), "s2ValU");
//         if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 2){
//             check(findParameter(unit, "s2ValU"), "s2ValU");
//             check(findParameter(unit, "s3ValU"), "s3ValU");
//         }
//     }
//     else if (ToolSet::stringToInt(unit["stimMode"], "In stimMode") == 1){
//         check(findParameter(unit, "s1ValI"), "s1ValI");
//         check(findParameter(unit, "s1Dur"), "s1Dur");
//         if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 1){
//             check(findParameter(unit, "s2ValI"), "s2ValI");
//             check(findParameter(unit, "s2Dur"), "s2DurI");
//         }
//         if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 2){
//             check(findParameter(unit, "s2ValI"), "s2ValI");
//             check(findParameter(unit, "s3ValI"), "s3ValI");
//             check(findParameter(unit, "s2Dur"), "s2Dur");
//             check(findParameter(unit, "s3Dur"), "s3Dur");
//         }
//     }
//     
//     if (!ToolSet::stringToInt(unit["protocolMode"], "In protocolMode")){
//         check(findParameter(unit, "s1Area"), "s1Area");
//     }
//     else if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 1){
//         check(findParameter(unit, "s2Area"), "s2Area");
//         check(findParameter(unit, "s2Time"), "s2Time");
//     }
//     else if (ToolSet::stringToInt(unit["protocolMode"], "In protocolMode") == 2){
//         check(findParameter(unit, "s3Area"), "s3Area");
//         check(findParameter(unit, "s3stimSeries"), "s3stimSeries");
//     }
//     
//     if (!findParameter(unit, "{Anisotropy}")){
//         check(!ToolSet::stringToInt(unit["transvAct"], "In transvAct"), "transvAct");
//     }
}

int Preprocessor::findParameter(std::map<std::string, std::string> unit, std::string parameterName)
{
    auto parameterIt = unit.find(parameterName);
    if (parameterIt == unit.end()){
        return 1; // error
    }
    else
        return 0;
}

void Preprocessor::check(int code, string message)
{
    if (code){
        cerr << "Config error: " << "points to " << message << endl;
    }
}
