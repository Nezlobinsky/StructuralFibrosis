import os
import sys
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import copy
import math
from numba import jit, njit, prange


def read_size(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("twigsLen"):
                return int(float(line_content[1].strip("\n")))

def read_fibDeg(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("fibDeg"):
                return float(line_content[1].strip("\n"))

def dist(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

def compute_length(trajectory, dr):
    length = 0
    for i in range(len(trajectory)-1):
        length += dist(trajectory[i], trajectory[i+1])*dr
    return length

@jit
def graph_out_4p_opt(matr, fibr, n, step):
    edges_list = []
    for k in range(n*n):
        if fibr[k]:
            continue
        i = k/400
        j = k%400
        if i == 0 or i == 399 or j == 0 or j == 399:
            continue
        if matr[(i-1)*n+j] == step and matr[i*n+j] == 0:
            matr[i*n+j] = step+1
            edges_list.append((i-1)*n+j)
            edges_list.append(i*n+j)
            continue
        if matr[(i+1)*n+j] == step and matr[i*n+j] == 0:
            matr[i*n+j] = step+1
            edges_list.append((i+1)*n+j)
            edges_list.append(i*n+j)
            continue
        if matr[i*n+(j-1)] == step and matr[i*n+j] == 0:
            matr[i*n+j] = step+1
            edges_list.append(i*n+(j-1))
            edges_list.append(i*n+j)
            continue
        if matr[i*n+(j+1)] == step and matr[i*n+j] == 0:
            matr[i*n+j] = step+1
            edges_list.append(i*n+(j+1))
            edges_list.append(i*n+j)
            continue
    return edges_list

data_dict = {}
n = 400
dr = 0.25

dirs = 0
for dir_ in os.listdir(sys.argv[1]):
    if dirs > 500:
        break
    dirs += 1
   
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue
    print dir_

    fibDg = read_fibDeg(os.path.join(sys.argv[1], dir_, "config.txt"))
    fibSz = read_size(os.path.join(sys.argv[1], dir_, "config.txt"))
    if not fibSz in data_dict:
        data_dict[fibSz] = {}
    if not fibDg in data_dict[fibSz]:
        data_dict[fibSz][fibDg] = []

    if len(data_dict[fibSz][fibDg]) > 5:
        continue

    path_lng = []
    for sp in range(0, 400, 10):
        fib_matrix = np.loadtxt(os.path.join(sys.argv[1], dir_, "fibrosis.txt"))

        res_matrix = np.copy(fib_matrix)

        res_matrix[fib_matrix == 1] = 0

        res_matrix[0, sp] = 1

        res_matrix = res_matrix.astype(int)
        fib_matrix = fib_matrix.astype(int)

        G = nx.Graph()

        iters = 1
        res_matrix = res_matrix.flatten("C").tolist()
        fib_matrix = fib_matrix.flatten("C").tolist()

        edges = []
        while sum(res_matrix[n*398:n*398+399]) == 0 and iters < 1200:
            #     res_matrix = graph_out_4p(res_matrix, fib_matrix,  n, iters)
            edges.extend(graph_out_4p_opt(res_matrix, fib_matrix, n, iters))
            iters += 1
        
        if iters >= 1200:
            continue 

        G.add_edges_from([(edges[i], edges[i+1]) for i in range(0, len(edges)-1, 2)])
        last = np.nonzero(res_matrix[n*398:n*398+399])[0][0]
        pth = nx.shortest_path(G, source=sp, target=398*n + last)

        coords = []
        for num in pth:
            coords.append([num/n, num%n])

        path_lng.append(compute_length(coords, dr)+dr)
    data_dict[fibSz][fibDg].append(np.mean(path_lng))

with open(os.path.join(sys.argv[1], "Shortest_path_script.txt"), "w") as fl:
    for szKey in sorted(data_dict):
        for fdKey in sorted(data_dict[szKey]):
            fl.write(str(szKey) + " " + str(fdKey) + " " + str(np.mean(data_dict[szKey][fdKey])) + " "
                     + str(np.max(data_dict[szKey][fdKey])) + " " + str(np.min(data_dict[szKey][fdKey])) + "\n")
