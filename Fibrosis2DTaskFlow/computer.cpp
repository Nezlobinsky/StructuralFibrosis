#include "computer.h"
#include "modeltnnp.h"
#include "modelap.h"

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <omp.h>

using std::vector;
using std::map;
using std::string;


void Computer::setTasks(vector<map<string, string>> configVec, vector<vector<vector<int>>> fibMatrixVec)
/*
 * Form the vector of tasks. 
 * Every task (model) require to put in a peace of config with parameters belongs to the task
 * and fibrosis matrix as std::vector even if fibrosis will be loaded from the state file.  
 */
{
    for (int i = 0; i < configVec.size(); ++i){
        if (configVec[i]["modelType"] == "AP"){
            tasksVec.push_back(new ModelAP(configVec[i]));
        }
        else if (configVec[i]["modelType"] == "TP06"){
            tasksVec.push_back(new ModelTNNP2D(configVec[i]));
        }
        tasksVec[i]->setFibrosisMatrix(fibMatrixVec[i]);
    }
    
    // default:
    taskThreads = 1;
    computationalThreads = 1;
}

void Computer::setNumberOfThreads(int taskThreads_, int computationalThreads_)
/*
 * Set the number of threads needed for the calculations.  
 */
{
    taskThreads = taskThreads_;
    computationalThreads = computationalThreads_;
}

void Computer::setStaticInterpreters(vector<map<string, string>> unitsVec)
/*
 * Form static interpreters vector. Each interpreter to each experiment (but not to each task). 
 */
{
    for (int i = 0; i < unitsVec.size(); ++i)
        staticInterpreterVec.push_back(new StaticInterpreter(unitsVec[i]));
}

void Computer::setDynamicInterpreters(std::vector<std::map<string, string>> configVec)
/*
 * Form dynamic interpreters vector. Each interpreter to each task.
 */
{
    for (int i = 0; i < configVec.size(); ++i){
        dynamicInterpreterVec.push_back(new DynamicInterpreter(configVec[i]));
    }
}

void Computer::bindDynamicInterpreters()
/*
 * Connect each task (model) with each dynamic interpreter and set the dynamic interpreter modes.
 */
{
    for (int i = 0; i < tasksVec.size(); ++i){
        tasksVec[i]->setDynamicInterpreter(dynamicInterpreterVec[i]);
    }
}

void Computer::run()
/*
 * Run the main calculations with calling of compute() method in each task (model). 
 */
{
    omp_set_nested(1);
    #pragma omp parallel for num_threads(taskThreads)
    for (int i = 0; i < tasksVec.size(); ++i){
        tasksVec[i]->setComputationalThreads(computationalThreads);
        tasksVec[i]->compute();
    }
}

void Computer::runStaticInterpreter()
/*
 * Run all static interpreters. Should be called after the main calculations (after the run() method). 
 */
{
    for (auto& interpreter : staticInterpreterVec){
        interpreter->interpret();
    }
}
