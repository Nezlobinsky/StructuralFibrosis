
#ifndef STATICWAVEPERIOD_H
#define STATICWAVEPERIOD_H

#include <map>
#include <string>
#include "staticstrategy.h"


class StaticWavePeriod : public StaticStrategy
{
public:
	StaticWavePeriod(std::map<std::string, std::string> unitMap);
	virtual ~StaticWavePeriod();

	void compute();
};

#endif /* STATICWAVEPERIOD_H */
