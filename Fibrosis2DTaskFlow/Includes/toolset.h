#ifndef TOOLSET_H
#define TOOLSET_H

#include <string>
#include <vector>

enum WAVE_DIRECTION{
        POSITIVE_Y, POSITIVE_X, NEGATIVE_X, NEGATIVE_Y,
};

class ToolSet
{
private:
    ToolSet() {};
    
public:    
    static void removeSpaces(std::string& str);
    static std::vector<std::string> splitToString(std::string targetString, std::string separator);
    static std::vector<double> splitToDouble(std::string targetString, std::string separator);
    static std::vector<int> splitToInt(std::string targetString, std::string separator);
    static int stringToInt(std::string targetString, std::string additionalMsg="");
    static double stringToDouble(std::string targetString, std::string additionalMsg="");
};
    
#endif
