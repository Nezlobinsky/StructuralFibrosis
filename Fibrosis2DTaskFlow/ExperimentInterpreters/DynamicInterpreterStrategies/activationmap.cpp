#include "activationmap.h"
#include "toolset.h"

#include <fstream>
#include <iostream>

using std::map;
using std::string;
using std::ofstream;
using std::endl;


ActivationMap::ActivationMap(map<string, string> config)
{
    sideNodes = ToolSet::stringToInt(config["sideNodes"], "In sideNodes (ActivationMap constructor)");
    transversalActFileFlag = ToolSet::stringToInt(config["TransvActFile"],
    		                                      "In TransvActFile (ActivationMap constructor)");
    resultsDir = config["dirPath"];

    arrivalTime = new double*[sideNodes];
    for (int i = 0; i < sideNodes; ++i){
        arrivalTime[i] = new double[sideNodes];
    }

    for (int i = 0; i < sideNodes; ++i){
    	for (int j = 0; j < sideNodes; ++j){
    		arrivalTime[i][j] = -1;
    	}
    }
}

ActivationMap::~ActivationMap()
{
    for (int i = 0; i < sideNodes; ++i){
         delete[] arrivalTime[i];
    }
    delete[] arrivalTime;
}

void ActivationMap::track(double** voltage, double t)
{
    for (int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j){
            if (voltage[i][j] >= -20. && arrivalTime[i][j] < 0.)
                arrivalTime[i][j] = t;
            else if (voltage[i][j] == -180.)
                arrivalTime[i][j] = -2;
        }
    }
}

void ActivationMap::write()
{
    string fileName;
    if (!transversalActFileFlag)
        fileName = "/arrivalTime.txt";
    else
        fileName = "/arrivalTime2.txt";

    ofstream aTime(resultsDir + fileName);
    for (int i = 0; i < sideNodes; ++i){
        for (int j = 0; j < sideNodes; ++j)
            aTime << arrivalTime[i][j] << " ";
        aTime << endl;
    }
    aTime.close();
}
