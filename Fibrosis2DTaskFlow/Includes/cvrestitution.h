#ifndef CVRESTITUTION_H
#define CVRESTITUTION_H

#include "dynamicstrategy.h"
#include "wavedirectiondetector.h"
#include "toolset.h"

#include <vector>
#include <string>
#include <utility>
#include <map>


class CVRestitution : public DynamicStrategy
{
public:
    CVRestitution(std::map<std::string, std::string> config);
    void track(double** voltage, double t);
    void write();

private:
    enum LINE_DIRECTION{
        NO = -1, PARALLEL_TO_X = 0, PARALLEL_TO_Y = 1,
    };
    int sideNodes;
    int detectorIndex;
    //int lineLevelIndex;  // row or column of detectors
    bool APDFlag;
    bool firstStimulus;
    double uThreshold;
    LINE_DIRECTION direction;
    double dr;

    std::string resultsDir;

    double DIstart;
    std::vector<double> timeActSeries;
    std::vector<std::pair<double, double>> cvRestitutionContainer;

};

#endif 
