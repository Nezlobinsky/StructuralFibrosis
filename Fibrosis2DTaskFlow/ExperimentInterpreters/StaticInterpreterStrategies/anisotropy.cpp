#include "anisotropy.h"
#include "wavefrontdetector.h"
#include "velocitydetector.h"
#include "wavedirectiondetector.h"
#include "toolset.h"

#include <vector>
#include <algorithm>
#include <utility>
#include <boost/filesystem.hpp>

using std::string;
using std::vector;
using std::pair;
using std::map;

using boost::filesystem::directory_iterator;


Anisotropy::Anisotropy(std::map<std::string, std::string> unitMap)
{
    fibMode          = ToolSet::stringToInt(unitMap["fibMode"], "In fibMode (Anisotropy constructor)");
    sideNodes        = ToolSet::stringToInt(unitMap["sideNodes"], "In sideNodes (Anisotropy constructor)");
    dr               = ToolSet::stringToDouble(unitMap["dr"], "In dr (Anisotropy constructor)");
    tMax             = ToolSet::stringToDouble(unitMap["tMax"], "In tMax (Anisotropy constructor)");
    rootDirPath      = unitMap["sectionName"];
    experimentsCount = ToolSet::stringToInt(unitMap["experimentsCount"], "In experimentsCount (Anisotropy constructor)");
}

Anisotropy::~Anisotropy()
{
}

void Anisotropy::compute()
{
    string fibDeg, twigsLen, distX, distY, s1Area;

    double conductivity1, conductivity2;
    WAVE_DIRECTION stimulusDirection;
    vector<pair<int, int>> frontVec1, frontVec2;
    vector<vector<double>> arrivalTime1, arrivalTime2;

    WaveFrontDetector waveFrontDetector = WaveFrontDetector(sideNodes, dr);
    VelocityDetector velocityDetector = VelocityDetector(sideNodes, dr);
    WaveDirectionDetector waveDirectionDetector = WaveDirectionDetector();

    resultsMap.clear();

    map<std::string, std::vector<double>> anisotropyMap;
    map<std::string, std::vector<double>> conductivity2Map;

    for (directory_iterator itr(rootDirPath); itr!=directory_iterator(); ++itr)
        {
            string filePath = rootDirPath + "/" + itr->path().filename().string();
            if (filePath.find(".txt") != string::npos)
                continue;

            if (fibMode < 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                fibDeg = readConfigParameter(filePath + "/config.txt", "fibDeg");
            }
            else if (fibMode == 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                distX    = readConfigParameter(filePath + "/config.txt", "distX");
                distY    = readConfigParameter(filePath + "/config.txt", "distY");
            }

            s1Area = readConfigParameter(filePath + "/config.txt", "s1Area");
            arrivalTime1 = readArrivalTime(filePath + "/arrivalTime.txt");
            arrivalTime2 = readArrivalTime(filePath + "/arrivalTime2.txt");

            // define wave front direction:
            waveDirectionDetector.setStimCoordinates(s1Area);
            stimulusDirection = waveDirectionDetector.define2DStimulusDirection();

            // define wave front (wave bound):
            waveFrontDetector.setTargetMesh(arrivalTime1);
            waveFrontDetector.setWaveDirection(stimulusDirection);
            frontVec1 = waveFrontDetector.findWaveFront();

            // compute wave velocity:
            velocityDetector.setTargetMesh(arrivalTime1);
            velocityDetector.setWaveDirection(stimulusDirection);
            conductivity1 = velocityDetector.computeVelocity(frontVec1, 0., tMax, true);

            // define wave front direction for the transversal activation:
            waveDirectionDetector.transversal();
            stimulusDirection = waveDirectionDetector.define2DStimulusDirection();

            // define wave front for the transversal activation (wave bound):
            waveFrontDetector.setTargetMesh(arrivalTime2);
            waveFrontDetector.setWaveDirection(stimulusDirection);
            frontVec2 = waveFrontDetector.findWaveFront();

            // compute velocity for the transversal activation:
            velocityDetector.setTargetMesh(arrivalTime2);
            velocityDetector.setWaveDirection(stimulusDirection);
            conductivity2 = velocityDetector.computeVelocity(frontVec2, 0., tMax, true);

            if (fibMode < 4){
                anisotropyMap[twigsLen + " " + fibDeg].push_back(conductivity2/conductivity1);
                conductivity2Map[twigsLen + " " + fibDeg].push_back(conductivity2);
            }
            else if (fibMode == 4){
                anisotropyMap[twigsLen + " " + distX + " " + distY].push_back(conductivity2/conductivity1);
                conductivity2Map[twigsLen + " " + distX + " " + distY].push_back(conductivity2);
            }

            arrivalTime1.clear();
            arrivalTime2.clear();
        }

    for (auto &elem : anisotropyMap){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }
    writeResults(rootDirPath + "/Anisotropy.txt");

    resultsMap.clear();
    for (auto &elem : conductivity2Map){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }
    writeResults(rootDirPath + "/Conductivity2.txt");
}
