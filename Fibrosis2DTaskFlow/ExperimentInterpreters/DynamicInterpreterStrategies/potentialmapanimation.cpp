#include "potentialmapanimation.h"
#include "toolset.h"

#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>

using std::map;
using std::string;
using std::ofstream;
using std::endl;
using std::to_string;

using boost::filesystem::exists;
using boost::filesystem::create_directory;


PotentialMapAnimation::PotentialMapAnimation(map<string, string> config)
{
    sideNodes = ToolSet::stringToInt(config["sideNodes"], "In sideNodes (PotentialMapAnimation constructor)");
    resultsDir = config["dirPath"];
    dt = ToolSet::stringToDouble(config["dt"], "In dt (PotentialMapAnimation constructor)");
    frameStep = ToolSet::stringToDouble(config["frameStep"], "In frameStep (PotentialMapAnimation constructor)");
    frameNumber = 0;

    time = 0;
}

PotentialMapAnimation::~PotentialMapAnimation()
{
}

void PotentialMapAnimation::track(double** voltage, double t)
{
    potentialMap = voltage;
}

void PotentialMapAnimation::write()
{
    string frameDir = resultsDir + "/Frames";
    if (time >= frameStep){
        if(!(exists(frameDir))){
            if (create_directory(frameDir)){
            }
        }
        ofstream frame(frameDir + "/" + to_string(frameNumber) + ".txt");
        for (int i = 0; i < sideNodes; ++i){
            for (int j = 0; j < sideNodes; ++j)
                frame << potentialMap[i][j] << " ";
            frame << endl;
        }
        frame.close();

        time = 0;
        frameNumber++;
    }
    time += dt;
}
