from os.path import join as ojoin
import os
import sys
import copy
import numpy as np
from numpy.linalg import eig, inv
import matplotlib.pyplot as plt


dir_name_list = []

fib_deg_list = []
twg_len_list = []

std_dict = {}


def merge_structures(dict_, list1, list2):
    for key1 in list1:
        dict_[key1] = {}
        for key2 in list2:
            dict_[key1][key2] = []

def read_config(file_name):
    global fib_deg_list, twg_len_list
    with open(file_name) as fl:
        for line in fl:
            line = line.strip().replace(" ", "")
            if line.startswith("sectionName"):
                dir_name_list.append(line.split("=")[1])
            elif line.startswith("fibDegSeries"):
                fib_deg_list = line.split("=")[1].split(":")
            elif line.startswith("twigsLenSeries"):
                twg_len_list = line.split("=")[1].split(":")

def read_parameter(file_name, parameter):
    with open(file_name) as fl:
        for line in fl:
            line = line.strip().replace(" ", "")
            if line.startswith(parameter):
                return line.split("=")[1]

def compute_std(wvfr):    
    return np.std(wvfr[:, 0])


config_path = sys.argv[1]
exp_set_path = os.path.split(config_path)[0]

read_config(config_path)

merge_structures(a_ellip_dict, fib_deg_list, twg_len_list)
merge_structures(b_ellip_dict, fib_deg_list, twg_len_list)

for dir_name in dir_name_list:
    for exp in os.listdir(ojoin(exp_set_path, dir_name)):
        wvfr = np.loadtxt(ojoin(exp_set_path, dir_name, exp, "WaveFront.txt"))
        std = compute_std(wvfr)

        fib_deg = str(float(read_parameter(ojoin(exp_set_path, dir_name, exp, "config.txt"),
                                 "fibDeg")))
        twg_len = str(int(float(read_parameter(ojoin(exp_set_path, dir_name, exp, "config.txt"),
                                 "twigsLen"))))

        std_dict[fib_deg][twg_len].append(std)

with open("STD.txt", "w") as fl:
    for key1 in std_dict:
        for key2 in std_dict[key1]:
            fl.write(key1 + " " + key2 + " " + str(np.mean(std_dict[key1][key2])) + "\n")


