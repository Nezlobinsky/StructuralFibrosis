#include "fibrosisarea.h"
#include "toolset.h"

#include <vector>
#include <algorithm>
#include <boost/filesystem.hpp>

using std::string;
using std::vector;
using std::map;

using boost::filesystem::directory_iterator;


FibrosisArea::FibrosisArea(std::map<std::string, std::string> unitMap)
{
    fibMode          = ToolSet::stringToInt(unitMap["fibMode"], "In fibMode (FibrosisArea constructor)");
    sideNodes        = ToolSet::stringToInt(unitMap["sideNodes"], "In sideNodes (FibrosisArea constructor)");
    dr               = ToolSet::stringToDouble(unitMap["dr"], "In dr (FibrosisArea constructor)");
    tMax             = ToolSet::stringToDouble(unitMap["tMax"], "In tMax (FibrosisArea constructor)");
    rootDirPath      = unitMap["sectionName"];
    experimentsCount = ToolSet::stringToInt(unitMap["experimentsCount"], "In experimentsCount (FibrosisArea constructor)");
}

FibrosisArea::~FibrosisArea()
{
}

void FibrosisArea::compute()
{
    string fibDeg, twigsLen, distX, distY;

    double fibrosisArea;

    resultsMap.clear();

    map<std::string, std::vector<double>> fibrosisAreaMap;

    for (directory_iterator itr(rootDirPath); itr!=directory_iterator(); ++itr){
        string filePath = rootDirPath + "/" + itr->path().filename().string();
        if (filePath.find(".txt") != string::npos)
            continue;

        if (fibMode < 4){
            twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
            fibDeg = readConfigParameter(filePath + "/config.txt", "fibDeg");
        }
        else if (fibMode == 4){
            twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
            distX    = readConfigParameter(filePath + "/config.txt", "distX");
            distY    = readConfigParameter(filePath + "/config.txt", "distY");
        }

        fibrosisArea = readValueFromFile(filePath + "/fibrosis_area.txt");

        if (fibMode < 4){
            fibrosisAreaMap[twigsLen + " " + fibDeg].push_back(fibrosisArea);
        }
        else if (fibMode == 4){
            fibrosisAreaMap[twigsLen + " " + distX + " " + distY].push_back(fibrosisArea);
        }
    }

    for (auto &elem : fibrosisAreaMap){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }

    writeResults(rootDirPath + "/FibrosisArea.txt");
}
