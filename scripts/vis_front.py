import sys
import numpy as np
import matplotlib.pyplot as plt


map_name = sys.argv[1]
front_name = sys.argv[2]

pmap = np.loadtxt(map_name)
front = np.loadtxt(front_name)


plt.imshow(pmap, cmap="seismic")
plt.plot(front[:,1], front[:,0], color="lime", linewidth=1.5)
plt.show()
