#ifndef STATICINTERPRETOR_H
#define STATICINTERPRETOR_H

#include <string>
#include <vector>
#include <map>

#include "staticstrategy.h"
#include "toolset.h"


class StaticInterpreterContext
{
public:
	//StaticInterpreterContext();
	void setStaticStrategy(StaticStrategy* staticStrategy){
		staticInterpreterStrategy = staticStrategy;
	}

	void execute(){
		staticInterpreterStrategy->compute();
	}

private:
    StaticStrategy* staticInterpreterStrategy;

};


class StaticInterpreter
/* 
 * Interpreter that works after all model calculations.
 * All measurements that must be performed on the base of computed results should be implemented here. 
 * 
 * Responsible for:
 * Conductivity calculation.
 * Anisotropy calculation.
 * Wave front length and complexity.
 */
{
    
private:
    std::vector<std::string> interpretModes;

    StaticInterpreterContext anisotropyContext;
    StaticInterpreterContext fibrosisAreaContext;
    StaticInterpreterContext frontComplexityContext;
    StaticInterpreterContext staticWavePeriodContext;
    StaticInterpreterContext waveConductivity;

public:
    StaticInterpreter(std::map<std::string, std::string> unitMap);

    void setInterpretVec(std::map<std::string, std::string> unitMap);
    void interpret();

};


#endif 
