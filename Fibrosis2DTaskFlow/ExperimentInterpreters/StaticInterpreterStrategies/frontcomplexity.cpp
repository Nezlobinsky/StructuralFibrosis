#include "frontcomplexity.h"
#include "wavefrontdetector.h"
#include "velocitydetector.h"
#include "wavedirectiondetector.h"
#include "toolset.h"

#include <vector>
#include <utility>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>

using std::string;
using std::vector;
using std::pair;
using std::map;
using std::ofstream;
using std::endl;

using boost::filesystem::directory_iterator;


FrontComplexity::FrontComplexity(std::map<std::string, std::string> unitMap)
{
    fibMode          = ToolSet::stringToInt(unitMap["fibMode"], "In fibMode (FrontComplexity constructor)");
    sideNodes        = ToolSet::stringToInt(unitMap["sideNodes"], "In sideNodes (FrontComplexity constructor)");
    dr               = ToolSet::stringToDouble(unitMap["dr"], "In dr (FrontComplexity constructor)");
    tMax             = ToolSet::stringToDouble(unitMap["tMax"], "In tMax (FrontComplexity constructor)");
    rootDirPath      = unitMap["sectionName"];
    experimentsCount = ToolSet::stringToInt(unitMap["experimentsCount"], "In experimentsCount (FrontComplexity constructor)");
}

FrontComplexity::~FrontComplexity()
{
}

double FrontComplexity::computeFrontLength(vector<pair<int, int>> frontVec)
{
    double length = 0.;
    // multiply with dr (spatial step) to get a real values
    for (int i = 0; i < frontVec.size()-1; ++i){
        length += sqrt(pow(frontVec[i].first*dr - frontVec[i+1].first*dr, 2) +
            pow(frontVec[i].second*dr - frontVec[i+1].second*dr, 2));
    }
    return length;
}

void FrontComplexity::writeWaveFront(std::string fileName, vector<pair<int, int>> frontVec)
{
    ofstream resFile(fileName);

    for (auto &elem : frontVec){
        resFile << elem.first << " " << elem.second << endl;
    }
    resFile.close();
}


void FrontComplexity::writerWaveFrontLength(std::string fileName, double length)
{
    ofstream resFile(fileName);

    resFile << length << endl;

    resFile.close();
}

void FrontComplexity::compute()
{
    string fibDeg, twigsLen, distX, distY, s1Area;

    vector<vector<double>> arrivalTime;
    vector<pair<int, int>> frontVec;
    WAVE_DIRECTION stimulusDirection;

    WaveFrontDetector waveFrontDetector = WaveFrontDetector(sideNodes, dr);
    VelocityDetector velocityDetector = VelocityDetector(sideNodes, dr);
    WaveDirectionDetector waveDirectionDetector = WaveDirectionDetector();

    resultsMap.clear();

    map<std::string, std::vector<double>> frontLengthMap;

    for (directory_iterator itr(rootDirPath); itr!=directory_iterator(); ++itr)
        {
            string filePath = rootDirPath + "/" + itr->path().filename().string();
            if (filePath.find(".txt") != string::npos)
                continue;

            if (fibMode < 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                fibDeg = readConfigParameter(filePath + "/config.txt", "fibDeg");
            }
            else if (fibMode == 4){
                twigsLen = readConfigParameter(filePath + "/config.txt", "twigsLen");
                distX    = readConfigParameter(filePath + "/config.txt", "distX");
                distY    = readConfigParameter(filePath + "/config.txt", "distY");
            }

            arrivalTime = readArrivalTime(filePath + "/arrivalTime.txt");
            s1Area = readConfigParameter(filePath + "/config.txt", "s1Area");

            // define wave front direction:
            waveDirectionDetector.setStimCoordinates(s1Area);
            stimulusDirection = waveDirectionDetector.define2DStimulusDirection();

            // define wave front (wave bound):
            waveFrontDetector.setWaveDirection(stimulusDirection);
            waveFrontDetector.setTargetMesh(arrivalTime);
            frontVec = waveFrontDetector.findWaveFront();

            writeWaveFront(filePath + "/WaveFront.txt", frontVec);
            writerWaveFrontLength(filePath + "/CurrentFrontLength.txt", computeFrontLength(frontVec));

            if (fibMode < 4){
                frontLengthMap[twigsLen + " " + fibDeg].push_back(computeFrontLength(frontVec));
            }
            else if (fibMode == 4){
                frontLengthMap[twigsLen + " " + distX + " " + distY].push_back(computeFrontLength(frontVec));
            }

            arrivalTime.clear();
        }

    for (auto &elem : frontLengthMap){
        double mean  = std::accumulate(elem.second.begin(), elem.second.end(), 0.)/experimentsCount;
        double upper = *std::max_element(elem.second.begin(), elem.second.end());
        double lower = *std::min_element(elem.second.begin(), elem.second.end());
        resultsMap[(elem.first)] = {mean, upper, lower};
    }

    writeResults(rootDirPath + "/FrontLength.txt");
}
