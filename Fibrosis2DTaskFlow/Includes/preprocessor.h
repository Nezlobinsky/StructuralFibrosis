#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>


class Preprocessor{
    
private:
    std::vector<std::map<std::string, std::string>>& experimentsVector;
    
    std::vector<std::string> minimalParametersVec;

    void buildExperimentsVector(std::map<std::string, std::string>& unit);
    void buildRandomPatternVector(std::map<std::string, std::string>& unit, int fibMode);
    void buildEquidistPatternVector(std::map<std::string, std::string>& unit, int fibMode);

public:
    Preprocessor(std::vector<std::map<std::string, std::string>>& experimentsVec);

    void process(std::vector<std::map<std::string, std::string>>& unitsVec);
    void checkUnitCorrectness(std::map<std::string, std::string> config);
    int findParameter(std::map<std::string, std::string> config, std::string parameterName);
    void check(int, std::string);

};

#endif
 
