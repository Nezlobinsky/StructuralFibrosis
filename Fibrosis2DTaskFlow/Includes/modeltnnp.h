#ifndef MODELTNNP_H
#define MODELTNNP_H

#include <string>
#include <map>

#include "modelinterface2d.h"
#include "toolset.h"

#define EPI
//#define ENDO
//#define MCELL


class ModelTNNP2D : public ModelInterface2D
{

public:
    ModelTNNP2D(std::map<std::string, std::string>& exp_config);
/* Initialization setters: */
    //void setModelParameters(std::map<std::string, double>);
    
private:
/* Initialization setters: */
    void setVariablesInitState();
    void loadVariablesInitState();
    void saveVariablesState();

/* Computational methods: */
    void compute();
    void computeLaplace();
    void computeCellsState();
    void computeI();
    void rewriteU();

/* Model variables: */
    double **uNext; // u on the next step 
    double **Cai;
    double **CaSR;
    double **CaSS;
    double **Nai;
    double **Ki;
    //states of voltage and time dependent gates
    //INa
    double **M_;
    double **H_;
    double **J_;
    //IKr
    //IKr1
    double **Xr1;
    //IKr2
    double **Xr2;
    //IKs
    double **Xs;
    //Ito1
    double **R_;
    double **S_;
    //ICa
    double **D_;
    double **F_;
    double **F2_;
    double **FCass;
    //Irel
    double **RR;
    double **OO;
    
/* Arrays memory allocation: */
    void allocateVariablesMemory();
    void freeVariablesMemory();

/* Constants */
    const double D_along = 0.154;

    //External concentrations
    const double Ko=5.4;
    const double Cao=2.0;
    const double Nao=140.0;

    //Intracellular volumes
    const double Vc=0.016404;
    const double Vsr=0.001094;
    const double Vss=0.00005468;

    //Calcium buffering dynamics
    const double Bufc=0.2;
    const double Kbufc=0.001;
    const double Bufsr=10.;
    const double Kbufsr=0.3;
    const double Bufss=0.4;
    const double Kbufss=0.00025;

    //Intracellular calcium flux dynamics
    const double Vmaxup=0.006375;
    const double Kup=0.00025;
    const double Vrel=0.102;//40.8;
    const double k1_=0.15;
    const double k2_=0.045;
    const double k3=0.060;
    const double k4=0.005;//0.000015;
    const double EC=1.5;
    const double maxsr=2.5;
    const double minsr=1.;
    const double Vleak=0.00036;
    const double Vxfer=0.0038;

    //Constants
    const double R=8314.472;
    const double F=96485.3415;
    const double T=310.0;
    const double RTONF=26.713760659695648;

    //Cellular capacitance         
    const double CAPACITANCE=0.185;

    //Parameters for currents
    //Parameters for IKr
    const double Gkr=0.153;
    //Parameters for Iks
    const double pKNa=0.03;
    #ifdef EPI
    const double Gks=0.392;
    #endif
    #ifdef ENDO
    const double Gks=0.392;
    #endif
    #ifdef MCELL
    const double Gks=0.098;
    #endif
    //Parameters for Ik1
    const double GK1=5.405;
    //Parameters for Ito
    #ifdef EPI
    const double Gto=0.294;
    #endif
    #ifdef ENDO
    const double Gto=0.073;
    #endif
    #ifdef MCELL
    const double Gto=0.294;
    #endif
    //Parameters for INa
    const double GNa=14.838;
    //Parameters for IbNa
    const double GbNa=0.00029;
    //Parameters for INaK
    const double KmK=1.0;
    const double KmNa=40.0;
    const double knak=2.724;
    //Parameters for ICaL
    const double GCaL=0.00003980;  
    //Parameters for IbCa
    const double GbCa=0.000592;
    //Parameters for INaCa
    const double knaca=1000;
    const double KmNai=87.5;
    const double KmCa=1.38;
    const double ksat=0.1;
    const double n_=0.35;
    //Parameters for IpCa
    const double GpCa=0.1238;
    const double KpCa=0.0005;
    //Parameters for IpK;
    const double GpK=0.0146;
};

#endif 
