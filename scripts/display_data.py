import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

font = {'size'   : 13}

matplotlib.rc('font', **font)


size_dict = {"1": [[], [], [], [], []], 
             "2": [[], [], [], [], []], 
             "3": [[], [], [], [], []], 
             "4": [[], [], [], [], []]}
#size_dict = {"2": [[], [], [], [], []], 
#             "3": [[], [], [], [], []],
#             "4": [[], [], [], []]}


with open(sys.argv[1]) as rfile:
    for line in rfile:
        line = line.split()
        size  = line[0].split(".")[0]
        prob  = float(line[1])*100
        value = float(line[2])/99.5
        max_v = float(line[3])
        min_v = float(line[4])
                
        size_dict[size][0].append(prob)
        size_dict[size][1].append(value)
        size_dict[size][2].append(max_v - value)
        size_dict[size][3].append(value - min_v)

if len(sys.argv) > 2:
    with open(sys.argv[2]) as rfile:
        for line in rfile:
            line = line.split()
            size  = line[0].split(".")[0]
            value = float(line[2])*100
                
            size_dict[size][4].append(value)
    
    for key in size_dict:
        plt.errorbar(size_dict[key][4],
                     size_dict[key][1], 
                     yerr=[size_dict[key][3], size_dict[key][2]], label="size " + key)

else:
    for key in sorted(size_dict):
        plt.errorbar(size_dict[key][0],
                     size_dict[key][1], 
                     label="size " + key)

plt.legend(loc=0)
plt.title("Normalized wavefront length (longitudinal)")
plt.ylabel("Length ratio")
plt.xlabel("Fibrosis area, %")
#plt.xlabel("Probability, %")
plt.grid()
plt.show()
