#include "configprocessor.h"
#include "preprocessor.h"
#include "dir_installation.h"
#include "fibrosismatrix.h"
#include "computer.h"
#include "toolset.h"

#include <iostream>
#include <map>
#include <string>
#include <boost/thread.hpp>
#include <cstdlib>

using std::string;
using std::vector;
using std::map;
using std::stod;
using std::stoi;

using boost::thread;


int main(int argc, char *argv[])
/*
 * Args:
 * [1] - config file name
 * [2] - threads count (optional), default = cpu count
 * [3] - threads per task (optional), default = 1
 */
{
    if (argc < 2){
        std::cout << "No config file was specified" << std::endl;
        return 1; 
    }
    
    int taskThreads;          // how many experiments may be computed at the same time
    int computationalThreads; // how many threads may be used for each task 
    
    if (argc == 2){
        // Get the number of cores on this machine
        taskThreads = thread::hardware_concurrency();
        computationalThreads = 1;
    }
    else if (argc == 3){
        taskThreads = stoi((string) argv[2]);
        computationalThreads = 1;
    }
    else if (argc == 4){
        taskThreads = stoi((string) argv[2]);
        computationalThreads = stoi((string) argv[3]);
    }
    
    string file_name(argv[1]);
    
    // all parameters from config for each experiment:
    vector<map<string, string>> experimentsVector;
    
    // all parameters from config for each experiment:
    vector<map<string, string>> unitsVector;

    auto reader = ConfigProcessor(unitsVector);

    auto preprocessor = Preprocessor(experimentsVector);
    
    auto dirInstallation = DirInstallation();
    auto fibrosisMatrix = FibrosisMatrix();

    auto computer = Computer();
    
    // Read config file:
    int readingErr = reader.Read(file_name);
    if (readingErr){
    	return EXIT_FAILURE;
    }
    
    // Prepare the experiments vector and check the correctness of the input data:
    preprocessor.process(unitsVector);    

    // Create directories for all tasks and place basic information about them (piece of config and a fibrosis matrix):  
    for (auto & config : experimentsVector)
    {   
        int dirInstErr = dirInstallation.createDirs(config);
        if (dirInstErr){
        	return EXIT_FAILURE;
        }

        int fibrMatrixErr = fibrosisMatrix.createAndWriteFibrMatrix(config);
        if (fibrMatrixErr){
        	return EXIT_FAILURE;
        }
    }

    computer.setTasks(experimentsVector, fibrosisMatrix.getFibrosisMatrixVec());
    computer.setDynamicInterpreters(experimentsVector);
    computer.setStaticInterpreters(unitsVector);
    computer.bindDynamicInterpreters();
    computer.setNumberOfThreads(taskThreads, computationalThreads);
    computer.run();
    computer.runStaticInterpreter();

    return 0;
}
