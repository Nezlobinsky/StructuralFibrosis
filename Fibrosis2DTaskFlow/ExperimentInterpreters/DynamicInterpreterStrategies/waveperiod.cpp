#include "waveperiod.h"
#include "toolset.h"

#include <iostream>
#include <fstream>
#include <algorithm>


using std::string;
using std::vector;
using std::pair;
using std::make_pair;
using std::ofstream;
using std::endl;


WavePeriod::WavePeriod(std::map<std::string, std::string> config)
{
    pointsMeshSize = 49; // number of detector for the spiral wave period

    sideNodes = ToolSet::stringToInt(config["sideNodes"], "In sideNodes (WavePeriod constructor)");
    resultsDir = config["dirPath"];

    // allocate and initialize mesh points detector (wave period):
    periodPoints = new pair<int, int>[pointsMeshSize];
    periodThresholdFlags = new bool[pointsMeshSize];
    int step = 60;
    int ind = 0;
    for (int i = 5; i < sideNodes-5 && ind < pointsMeshSize; i+=step){
        for (int j = 5; j < sideNodes-5 && ind < pointsMeshSize; j+=step){
            periodPoints[ind].first = i;
            periodPoints[ind].second = j;
            vector<pair<double, double>> vec;
            timeAndUValuesMesh.push_back(vec);
            periodThresholdFlags[ind] = false;
            ind++;
        }
    }

    pointsMeshSize = ind;
}

WavePeriod::~WavePeriod()
{
    delete[] periodPoints;
    delete[] periodThresholdFlags; 
}

void WavePeriod::track(double** voltage, double t)
{
    for (int i = 0; i < pointsMeshSize; ++i){
        // using 0. as a threshold for the period measurements
        int x = periodPoints[i].first;
        int y = periodPoints[i].second;
        double voltageValue = voltage[x][y];
        if (voltageValue > 0. && !periodThresholdFlags[i]){
            pair<double, double> timeAndU = make_pair(t, voltageValue);
            timeAndUValuesMesh[i].push_back(timeAndU);
            periodThresholdFlags[i] = true;
        }
        if (voltageValue < 0.)
            periodThresholdFlags[i] = false;
    }
}

//void WavePeriod::writeMeshPointsPeriod(string fileName)
//{
//    ofstream periodPointsMeshFile(fileName);
//    for (int i = 0; i < timeAndUValuesMesh.size(); ++i){
//        for (int j = 0; j < timeAndUValuesMesh[i].size(); ++j){
//            periodPointsMeshFile << "Number: " << i << " Coordinates: " << periodPoints[i].first << " " << periodPoints[i].second
//            << " Time: " << timeAndUValuesMesh[i][j].first << "   Uval:" << timeAndUValuesMesh[i][j].second << endl;
//        }
//        periodPointsMeshFile << "----" << endl << "----" << endl;
//    }
//    periodPointsMeshFile.close();
//}

void WavePeriod::write()
{
    ofstream periodFile(resultsDir + "/wave_period.txt");
    vector<double> periodInPointsVec;
    for (int i = 0; i < timeAndUValuesMesh.size(); ++i){
        double period = computePeriod(timeAndUValuesMesh[i]);
        if (period >= 0.){
            periodInPointsVec.push_back(period);
        }
    }

    double period = std::accumulate(periodInPointsVec.begin(), periodInPointsVec.end(), 0.)/periodInPointsVec.size();
    periodFile << period << endl;
    periodFile.close();
}

double WavePeriod::computePeriod(vector<pair<double, double>> timeAndUVec)
{
    if (timeAndUVec.size() < 3){
        return -1; // not enought elements (miss two stimulus at the begin)
    }

    vector<double> periodVec;
    for (int i = 2; i < timeAndUVec.size()-1; ++i){
        periodVec.push_back(timeAndUVec[i+1].first -
                            timeAndUVec[i].first);
    }

    return std::accumulate(periodVec.begin(), periodVec.end(), 0)/periodVec.size();
}

