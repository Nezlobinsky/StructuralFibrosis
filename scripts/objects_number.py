import os
import sys
import numpy as np
import matplotlib.pyplot as plt


def read_size(fname):
    with open(fname, "r") as fl:
        for line in fl:
            line_content = line.split("=")
            if line_content[0].startswith("twigsLen"):
                return int(float(line_content[1].strip("\n")))

def caclulate_objects(matr, size):
    n = 400
    objects = 0
    for i in range(n):
        points = 0
        for j in range(n):
            if matr[i, j]:
                points += 1
            if points == size:
                objects += 1
                points = 0
        if points > 0:
            objects += 1
    return objects

for dir_ in os.listdir(sys.argv[1]):
    if not os.path.isdir(os.path.join(sys.argv[1], dir_)):
        continue
    matr = np.loadtxt(os.path.join(sys.argv[1], dir_, "fibrosis.txt"))
    size = read_size(os.path.join(sys.argv[1], dir_, "config.txt"))
    with open(os.path.join(sys.argv[1], dir_, "objects.txt"), "w") as fl:
        fl.write(str(caclulate_objects(matr, size)))
